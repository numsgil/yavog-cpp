uniform sampler2D tDepth;

#ifdef VERTEX
layout(location = 0) in vec2 in_pos;
layout(location = 1) in vec2 in_tex;

out vec2 vs_tex;

void main()
{
	gl_Position = vec4(in_pos, 0, 1);
	vs_tex = in_tex;
}
#endif

#ifdef FRAGMENT
in vec2 vs_tex;

out vec4 frag_color;

void main()
{
	frag_color = vec4(vec3(texture(tDepth, vs_tex).r), 1.0f);
}
#endif
