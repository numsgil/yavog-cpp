struct Material_t
{
    sampler2D diffuse;
    vec3      specular;
    float     shininess;
};

struct Light_t
{
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

uniform vec3 vView;
uniform Material_t Material;
uniform Light_t Light;

vec3 Ambient(vec2 uv)
{
    return Light.ambient * texture(Material.diffuse, uv).rgb;
}

vec3 Diffuse(vec3 normal, vec2 uv)
{
    vec3 lightDir = normalize(-Light.direction);
    float diff = max(dot(normalize(normal), lightDir), 0.0);
    return Light.diffuse * diff * texture(Material.diffuse, uv).rgb;
}

vec3 Specular(vec3 position, vec3 normal)
{
    vec3 lightDir = normalize(-Light.direction);
    vec3 viewDir = normalize(vView - position);
    vec3 reflectDir = reflect(-lightDir, normalize(normal));
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), Material.shininess);
    return Light.specular * spec * Material.specular;
}
