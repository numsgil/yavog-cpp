#pragma import Matrices
#pragma import Lighting

#ifdef VERTEX
layout(location = 0) in vec3 in_pos;
layout(location = 1) in vec3 in_nor;
layout(location = 2) in vec2 in_tex;

out vec3 vs_pos;
out vec3 vs_nor;
out vec2 vs_tex;

void main()
{
	gl_Position = matProj * matView * matModel * vec4(in_pos, 1);
	vs_pos = vec3(matModel * vec4(in_pos, 1));
	vs_nor = mat3(inverse(transpose(matModel))) * in_nor;
	vs_tex = in_tex;
}
#endif

#ifdef FRAGMENT
in vec3 vs_pos;
in vec3 vs_nor;
in vec2 vs_tex;

out vec4 frag_color;

void main()
{
	frag_color = vec4(Ambient(vs_tex) + Diffuse(vs_nor, vs_tex) + Specular(vs_pos, vs_nor), 1.0f);
}
#endif
