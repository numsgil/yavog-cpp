#pragma import Matrices

#ifdef VERTEX
layout(location = 0) in vec3 in_pos;

void main()
{
	gl_Position = matProj * matView * matModel * vec4(in_pos, 1);
}
#endif

#ifdef FRAGMENT
void main() { }
#endif
