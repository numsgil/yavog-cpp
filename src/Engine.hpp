#ifndef YAVOG_CPP_ENGINE_HPP
#define YAVOG_CPP_ENGINE_HPP

#include <glbinding/gl40core/gl.h>
#include <g3log/logworker.hpp>
#include <anax/anax.hpp>
#include <type_traits>
#include <glm/glm.hpp>
#include <vector>
#include <map>

#include "Capabilities.hpp"
#include "InputEnums.hpp"
#include "ThreadPool.hpp"
#include "Renderer.hpp"
#include "Settings.hpp"

using namespace gl40core;

class AutoWorld : public anax::World
{
public:
	AutoWorld();

	virtual ~AutoWorld();

	AutoWorld(const AutoWorld& world) = delete;

	AutoWorld(AutoWorld&& world) = delete;

	AutoWorld& operator=(const AutoWorld&) = delete;

	AutoWorld& operator=(AutoWorld&&) = delete;

	template<class T, typename... Args>
	T& add_system(Args&& ... args)
	{
		static_assert(std::is_base_of<anax::detail::BaseSystem, T>(), "Template argument does not inherit from BaseSystem");
		T* ret = new T(args...);
		addSystem(*ret);
		_systems[anax::SystemTypeId<T>()] = ret;
		return *ret;
	}

	template<class T>
	void remove_system()
	{
		static_assert(std::is_base_of<anax::detail::BaseSystem, T>(), "Template argument does not inherit from BaseSystem");
		removeSystem<T>();
		auto i = anax::SystemTypeId<T>();
		delete _systems[i];
		_systems.erase(i);
	}

	template<class T>
	T& system()
	{
		static_assert(std::is_base_of<anax::detail::BaseSystem, T>(), "Template argument does not inherit from BaseSystem");
		auto i = anax::SystemTypeId<T>();
		if (_systems.find(i) == _systems.end()) throw std::runtime_error("System does not exist in world");
		return *dynamic_cast<T*>(_systems[i]);
	}

protected:
	std::map<anax::detail::TypeId, anax::detail::BaseSystem*> _systems;
};

class Engine
{
	static Engine* _inst;

public:
	static Engine& instance();

	Engine();

	virtual ~Engine();

	Engine(Engine const& other) = delete;

	Engine(Engine&& other) = delete;

	Engine& operator=(Engine const& other) = delete;

	Engine& operator=(Engine&& other) = delete;

	virtual void create();

	virtual void destroy();

	virtual void exit();

	virtual Settings& settings();

	virtual AutoWorld& world();

	virtual Capabilities& capabilities();

	virtual Renderer& renderer();

	virtual bool is_running() const;

	virtual void swap_buffers() const;

	virtual bool is_key_down(Key const& code) const;

	virtual MouseMode mouse_mode() const;

	virtual void mouse_mode(MouseMode const& mode);

	virtual glm::vec2 mouse_position() const;

	virtual bool is_mouse_down(Mouse const& mouse) const;

	virtual glm::ivec2 window_size() const;

	virtual glm::ivec2 framebuffer_size() const;

	virtual float time() const;

	template<class F, class... Args>
	auto add_task(F&& task, Args&& ... args) -> std::future<typename std::result_of<F(Args...)>::type>
	{
		return _pool->enqueue(std::forward<F>(task), std::forward<Args>(args)...);
	};

protected:
	Settings* _settings;

	struct GLFWwindow* _window;
	AutoWorld* _world;
	ThreadPool* _pool;

	Capabilities* _caps;
	Renderer* _renderer;

	std::unique_ptr<g3::LogWorker> _log;
};

#endif //YAVOG_CPP_ENGINE_HPP
