#ifndef YAVOG_CPP_OCTREEBRANCH_HPP
#define YAVOG_CPP_OCTREEBRANCH_HPP

#include <array>

#include "OctreeNode.hpp"

class OctreeBranch : public OctreeNode
{
public:
	OctreeBranch(glm::ivec3 const& origin, glm::uint32 half_length);

	virtual ~OctreeBranch();

	OctreeBranch(OctreeBranch const& other) = delete;

	OctreeBranch(OctreeBranch&& other) = delete;

	OctreeBranch& operator=(OctreeBranch const& other) = delete;

	OctreeBranch& operator=(OctreeBranch&& other) = delete;

	virtual bool is_leaf() const override
	{ return false; };

	virtual bool is_dirty() const override;

	virtual bool is_empty() const override;

	virtual glm::uint16 at(glm::ivec3 const& position) const override;

	virtual void at(glm::ivec3 const& position, glm::uint16 value) override;

	virtual void traverse(std::function<void(OctreeNode*)> const& fn) override;

	virtual void create_children();

protected:
	static int octant_containing(glm::ivec3 const& point, glm::ivec3 const& origin);

	virtual void create_child(int octant);

	std::array<OctreeNode*, 8> _children;
};

#endif //YAVOG_CPP_OCTREEBRANCH_HPP
