#ifndef YAVOG_CPP_REGION_HPP
#define YAVOG_CPP_REGION_HPP

#include <glm/glm.hpp>

class Region
{
public:
	Region();

	Region(glm::ivec3 const& min, glm::ivec3 const& max);

	Region(Region const& other);

	Region(Region&& other);

	Region& operator=(Region const& other);

	Region& operator=(Region&& other);

	glm::ivec3 min() const
	{ return _min; }

	void min(glm::ivec3 const& min)
	{ _min = min; }

	glm::ivec3 max() const
	{ return _max; }

	void max(glm::ivec3 const& max)
	{ _max = max; }

	bool is_null() const
	{ return _min.x > _max.x || _min.y > _max.y || _min.z > _max.z; }

protected:
	glm::ivec3 _min, _max;
};

#endif //YAVOG_CPP_REGION_HPP
