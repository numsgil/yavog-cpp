#include "OctreeLeaf.hpp"

using namespace glm;

OctreeLeaf::OctreeLeaf(ivec3 const& origin) : OctreeNode(origin, HalfLeafSize), _dirty(true), _empty(true)
{
	for (int i = 0; i < (int) _data.size(); i++) _data[i] = 0;
}

OctreeLeaf::~OctreeLeaf()
{
}

uint16 OctreeLeaf::at(ivec3 const& position) const
{
	ivec3 index = position - (_origin - (int) _half_length);
	return _data.at((std::size_t) IndexAt(index));
}

void OctreeLeaf::at(ivec3 const& position, uint16 value)
{
	ivec3 index = position - (_origin - (int) _half_length);
	_data.at((std::size_t) IndexAt(index)) = value;
	if(value > 0) _empty = false;
}

void OctreeLeaf::traverse(std::function<void(OctreeNode*)> const& fn)
{
	fn(this);
}
