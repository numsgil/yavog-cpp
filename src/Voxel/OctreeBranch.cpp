#include <g3log/g3log.hpp>

#include "OctreeBranch.hpp"
#include "OctreeLeaf.hpp"

using namespace glm;

bool is_pot(uint32 value)
{
	return ((value != 0) && !(value & (value - 1)));
}

int OctreeBranch::octant_containing(ivec3 const& point, ivec3 const& origin)
{
	return 0 | (point.x >= origin.x ? 4 : 0) | (point.y >= origin.y ? 2 : 0) | (point.z >= origin.z ? 1 : 0);
}

OctreeBranch::OctreeBranch(ivec3 const& origin, uint32 half_length) : OctreeNode(origin, half_length)
{
	CHECK(is_pot(half_length) && half_length >= LeafSize) << "The Branch's Half Side Length must be a power of two and greater than " << HalfLeafSize;
	for (int i = 0; i < 8; i++) _children[i] = nullptr;
}

OctreeBranch::~OctreeBranch()
{
	for (int i = 0; i < 8; i++)
		if (_children[i] != nullptr)
		{
			delete _children[i];
			_children[i] = nullptr;
		}
}

bool OctreeBranch::is_dirty() const
{
	bool dirty = false;
	for (int i = 0; i < 8; i++)
		if (_children[i] != nullptr && _children[i]->is_dirty())
			dirty = true;
	return dirty;
}

bool OctreeBranch::is_empty() const
{
	bool empty = true;
	for (int i = 0; i < 8; i++)
		if (_children[i] != nullptr && _children[i]->is_empty())
			empty = false;
	return empty;
}

uint16 OctreeBranch::at(ivec3 const& position) const
{
	int octant = octant_containing(position, _origin);
	if (_children[octant] == nullptr) return 0;
	return _children[octant]->at(position);
}

void OctreeBranch::at(ivec3 const& position, uint16 value)
{
	int octant = octant_containing(position, _origin);
	if (_children[octant] == nullptr && value == 0) return;

	if (_children[octant] == nullptr)
		create_child(octant);

	_children[octant]->at(position, value);
}

void OctreeBranch::traverse(std::function<void(OctreeNode*)> const& fn)
{
	fn(this);

	for (int i = 0; i < 8; i++)
		if (_children[i] != nullptr)
			_children[i]->traverse(fn);
}

void OctreeBranch::create_children()
{
	for (int i = 0; i < 8; i++)
	{
		create_child(i);
		auto child = dynamic_cast<OctreeBranch*>(_children[i]);
		if (child != nullptr)
			child->create_children();
	}
}

void OctreeBranch::create_child(int octant)
{
	if (_children[octant] != nullptr) return;

	ivec3 new_origin = _origin + ivec3(
			(int) _half_length / (octant & 4 ? 2 : -2),
			(int) _half_length / (octant & 2 ? 2 : -2),
			(int) _half_length / (octant & 1 ? 2 : -2)
	);
	uint32 new_half_length = _half_length / 2;
	_children[octant] = new_half_length > HalfLeafSize ? (OctreeNode*) new OctreeBranch(new_origin, new_half_length)
	                                                   : (OctreeNode*) new OctreeLeaf(new_origin);
}
