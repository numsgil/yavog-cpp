#include "Region.hpp"

using namespace glm;

Region::Region() : _min(1, 1, 1), _max(-1, -1, -1)
{ }

Region::Region(ivec3 const& min, ivec3 const& max) : _min(min), _max(max)
{ }

Region::Region(Region const& other) : _min(other._min), _max(other._max)
{ }

Region::Region(Region&& other) : _min(other._min), _max(other._max)
{
	other._min = ivec3(1, 1, 1);
	other._max = ivec3(-1, -1, -1);
}

Region& Region::operator=(Region const& other)
{
	_min = other._min;
	_max = other._max;
	return *this;
}

Region& Region::operator=(Region&& other)
{
	_min = other._min;
	_max = other._max;

	other._min = ivec3(1, 1, 1);
	other._max = ivec3(-1, -1, -1);
	return *this;
}
