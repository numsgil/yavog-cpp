#ifndef YAVOG_CPP_OCTREENODE_HPP
#define YAVOG_CPP_OCTREENODE_HPP

#include <glm/glm.hpp>
#include <functional>

class OctreeNode
{
public:
	OctreeNode(glm::ivec3 const& origin, glm::uint32 half_length) : _origin(origin), _half_length(half_length)
	{ }

	virtual ~OctreeNode()
	{ }

	OctreeNode(OctreeNode const& other) = delete;

	OctreeNode(OctreeNode&& other) = delete;

	OctreeNode& operator=(OctreeNode const& other) = delete;

	OctreeNode& operator=(OctreeNode&& other) = delete;

	virtual glm::ivec3 origin() const final { return _origin; }

	virtual glm::uint32 half_length() const final { return _half_length; }

	virtual bool is_leaf() const = 0;

	virtual bool is_dirty() const = 0;

	virtual bool is_empty() const = 0;

	virtual glm::uint16 at(int x, int y, int z) const final
	{ return at(glm::ivec3(x, y, z)); }

	virtual glm::uint16 at(glm::ivec3 const& position) const = 0;

	virtual void at(int x, int y, int z, glm::uint16 value) final
	{ at(glm::ivec3(x, y, z), value); }

	virtual void at(glm::ivec3 const& position, glm::uint16 value) = 0;

	virtual void traverse(std::function<void(OctreeNode*)> const& fn) = 0;

protected:
	glm::ivec3 _origin;
	glm::uint32 _half_length;
};

#endif //YAVOG_CPP_OCTREENODE_HPP
