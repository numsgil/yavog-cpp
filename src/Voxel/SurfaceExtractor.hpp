#ifndef YAVOG_CPP_SURFACEEXTRACTOR_HPP
#define YAVOG_CPP_SURFACEEXTRACTOR_HPP

#include "OctreeNode.hpp"
#include "Region.hpp"

#include <vector>

constexpr float BlockSize = 1.f;
constexpr float BlockHalfSize = BlockSize / 2.f;

struct SurfaceVertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 uv;
};

class SurfaceExtractor
{
public:
	SurfaceExtractor(OctreeNode* node);

	virtual ~SurfaceExtractor();

	SurfaceExtractor(SurfaceExtractor const& other) = delete;

	SurfaceExtractor(SurfaceExtractor&& other) = delete;

	SurfaceExtractor& operator=(SurfaceExtractor const& other) = delete;

	SurfaceExtractor& operator=(SurfaceExtractor&& other) = delete;

	virtual std::vector<SurfaceVertex> extract(Region const& region);

protected:
	OctreeNode* _node;
};

#endif //YAVOG_CPP_SURFACEEXTRACTOR_HPP
