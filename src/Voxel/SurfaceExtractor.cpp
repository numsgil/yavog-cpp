#include <g3log/g3log.hpp>

#include "SurfaceExtractor.hpp"

using namespace std;
using namespace glm;

enum class Face
{
	Up, Down,       // Y
	North, South,   // Z
	East, West      // X
};

template<Face>
void generate_face(ivec3 const& position, ivec3 const& origin, vector<SurfaceVertex>& vertices);

// Y+
template<>
void generate_face<Face::Up>(ivec3 const& position, ivec3 const& origin, vector<SurfaceVertex>& vertices)
{
	ivec3 p = position - origin;

	SurfaceVertex v0{
			vec3(BlockHalfSize + p.x * BlockSize, BlockHalfSize + p.y * BlockSize, BlockHalfSize + p.z * BlockSize),
			vec3(0, 1, 0),
			vec2(1, 0)
	};

	SurfaceVertex v1{
			vec3(-BlockHalfSize + p.x * BlockSize, BlockHalfSize + p.y * BlockSize, BlockHalfSize + p.z * BlockSize),
			vec3(0, 1, 0),
			vec2(0, 0)
	};

	SurfaceVertex v2{
			vec3(BlockHalfSize + p.x * BlockSize, BlockHalfSize + p.y * BlockSize, -BlockHalfSize + p.z * BlockSize),
			vec3(0, 1, 0),
			vec2(1, 1)
	};

	SurfaceVertex v3{
			vec3(-BlockHalfSize + p.x * BlockSize, BlockHalfSize + p.y * BlockSize, -BlockHalfSize + p.z * BlockSize),
			vec3(0, 1, 0),
			vec2(0, 1)
	};

	vertices.push_back(v0);
	vertices.push_back(v3);
	vertices.push_back(v1);

	vertices.push_back(v0);
	vertices.push_back(v2);
	vertices.push_back(v3);
}

// Y-
template<>
void generate_face<Face::Down>(ivec3 const& position, ivec3 const& origin, vector<SurfaceVertex>& vertices)
{
	ivec3 p = position - origin;

	SurfaceVertex v0{
			vec3(BlockHalfSize + p.x * BlockSize, -BlockHalfSize + p.y * BlockSize, BlockHalfSize + p.z * BlockSize),
			vec3(0, -1, 0),
			vec2(0, 0)
	};

	SurfaceVertex v1{
			vec3(-BlockHalfSize + p.x * BlockSize, -BlockHalfSize + p.y * BlockSize, BlockHalfSize + p.z * BlockSize),
			vec3(0, -1, 0),
			vec2(1, 0)
	};

	SurfaceVertex v2{
			vec3(BlockHalfSize + p.x * BlockSize, -BlockHalfSize + p.y * BlockSize, -BlockHalfSize + p.z * BlockSize),
			vec3(0, -1, 0),
			vec2(0, 1)
	};

	SurfaceVertex v3{
			vec3(-BlockHalfSize + p.x * BlockSize, -BlockHalfSize + p.y * BlockSize, -BlockHalfSize + p.z * BlockSize),
			vec3(0, -1, 0),
			vec2(1, 1)
	};

	vertices.push_back(v0);
	vertices.push_back(v1);
	vertices.push_back(v3);

	vertices.push_back(v0);
	vertices.push_back(v3);
	vertices.push_back(v2);
}

// Z+
template<>
void generate_face<Face::North>(ivec3 const& position, ivec3 const& origin, vector<SurfaceVertex>& vertices)
{
	ivec3 p = position - origin;

	SurfaceVertex v0{
			vec3(BlockHalfSize + p.x * BlockSize, BlockHalfSize + p.y * BlockSize, BlockHalfSize + p.z * BlockSize),
			vec3(0, 0, 1),
			vec2(1, 0)
	};

	SurfaceVertex v1{
			vec3(-BlockHalfSize + p.x * BlockSize, BlockHalfSize + p.y * BlockSize, BlockHalfSize + p.z * BlockSize),
			vec3(0, 0, 1),
			vec2(1, 1)
	};

	SurfaceVertex v2{
			vec3(BlockHalfSize + p.x * BlockSize, -BlockHalfSize + p.y * BlockSize, BlockHalfSize + p.z * BlockSize),
			vec3(0, 0, 1),
			vec2(0, 0)
	};

	SurfaceVertex v3{
			vec3(-BlockHalfSize + p.x * BlockSize, -BlockHalfSize + p.y * BlockSize, BlockHalfSize + p.z * BlockSize),
			vec3(0, 0, 1),
			vec2(0, 1)
	};

	vertices.push_back(v0);
	vertices.push_back(v1);
	vertices.push_back(v3);

	vertices.push_back(v0);
	vertices.push_back(v3);
	vertices.push_back(v2);
}

// Z-
template<>
void generate_face<Face::South>(ivec3 const& position, ivec3 const& origin, vector<SurfaceVertex>& vertices)
{
	ivec3 p = position - origin;

	SurfaceVertex v0{
			vec3(BlockHalfSize + p.x * BlockSize, BlockHalfSize + p.y * BlockSize, -BlockHalfSize + p.z * BlockSize),
			vec3(0, 0, -1),
			vec2(0, 0)
	};

	SurfaceVertex v1{
			vec3(-BlockHalfSize + p.x * BlockSize, BlockHalfSize + p.y * BlockSize, -BlockHalfSize + p.z * BlockSize),
			vec3(0, 0, -1),
			vec2(0, 1)
	};

	SurfaceVertex v2{
			vec3(BlockHalfSize + p.x * BlockSize, -BlockHalfSize + p.y * BlockSize, -BlockHalfSize + p.z * BlockSize),
			vec3(0, 0, -1),
			vec2(1, 0)
	};

	SurfaceVertex v3{
			vec3(-BlockHalfSize + p.x * BlockSize, -BlockHalfSize + p.y * BlockSize, -BlockHalfSize + p.z * BlockSize),
			vec3(0, 0, -1),
			vec2(1, 1)
	};

	vertices.push_back(v0);
	vertices.push_back(v3);
	vertices.push_back(v1);

	vertices.push_back(v0);
	vertices.push_back(v2);
	vertices.push_back(v3);
}

// X-
template<>
void generate_face<Face::East>(ivec3 const& position, ivec3 const& origin, vector<SurfaceVertex>& vertices)
{
	ivec3 p = position - origin;

	SurfaceVertex v0{
			vec3(-BlockHalfSize + p.x * BlockSize, BlockHalfSize + p.y * BlockSize, BlockHalfSize + p.z * BlockSize),
			vec3(-1, 0, 0),
			vec2(1, 0)
	};

	SurfaceVertex v1{
			vec3(-BlockHalfSize + p.x * BlockSize, -BlockHalfSize + p.y * BlockSize, BlockHalfSize + p.z * BlockSize),
			vec3(-1, 0, 0),
			vec2(0, 0)
	};

	SurfaceVertex v2{
			vec3(-BlockHalfSize + p.x * BlockSize, BlockHalfSize + p.y * BlockSize, -BlockHalfSize + p.z * BlockSize),
			vec3(-1, 0, 0),
			vec2(1, 1)
	};

	SurfaceVertex v3{
			vec3(-BlockHalfSize + p.x * BlockSize, -BlockHalfSize + p.y * BlockSize, -BlockHalfSize + p.z * BlockSize),
			vec3(-1, 0, 0),
			vec2(0, 1)
	};

	vertices.push_back(v0);
	vertices.push_back(v3);
	vertices.push_back(v1);

	vertices.push_back(v0);
	vertices.push_back(v2);
	vertices.push_back(v3);
}

// X+
template<>
void generate_face<Face::West>(ivec3 const& position, ivec3 const& origin, vector<SurfaceVertex>& vertices)
{
	ivec3 p = position - origin;

	SurfaceVertex v0{
			vec3(BlockHalfSize + p.x * BlockSize, BlockHalfSize + p.y * BlockSize, BlockHalfSize + p.z * BlockSize),
			vec3(1, 0, 0),
			vec2(0, 0)
	};

	SurfaceVertex v1{
			vec3(BlockHalfSize + p.x * BlockSize, -BlockHalfSize + p.y * BlockSize, BlockHalfSize + p.z * BlockSize),
			vec3(1, 0, 0),
			vec2(1, 0)
	};

	SurfaceVertex v2{
			vec3(BlockHalfSize + p.x * BlockSize, BlockHalfSize + p.y * BlockSize, -BlockHalfSize + p.z * BlockSize),
			vec3(1, 0, 0),
			vec2(0, 1)
	};

	SurfaceVertex v3{
			vec3(BlockHalfSize + p.x * BlockSize, -BlockHalfSize + p.y * BlockSize, -BlockHalfSize + p.z * BlockSize),
			vec3(1, 0, 0),
			vec2(1, 1)
	};

	vertices.push_back(v0);
	vertices.push_back(v1);
	vertices.push_back(v3);

	vertices.push_back(v0);
	vertices.push_back(v3);
	vertices.push_back(v2);
}

SurfaceExtractor::SurfaceExtractor(OctreeNode* node) : _node(node)
{
	CHECK(_node != nullptr) << "Node needs to be non-null";
}

SurfaceExtractor::~SurfaceExtractor()
{ }

vector<SurfaceVertex> SurfaceExtractor::extract(Region const& region)
{
	CHECK(!region.is_null()) << "Region needs to be valid";
	ivec3 min = region.min(), max = region.max();
	vector<SurfaceVertex> data;

	int hl = static_cast<int>(_node->half_length());

	if (min.x < -hl) min.x = -hl;
	if (min.y < -hl) min.y = -hl;
	if (min.z < -hl) min.z = -hl;

	if (max.x > hl) max.x = hl;
	if (max.y > hl) max.y = hl;
	if (max.z > hl) max.z = hl;

	LOG(INFO) << "Extracting surface from ("
	          << min.x << ", " << min.y << ", " << min.z << ") to ("
	          << max.x << ", " << max.y << ", " << max.z << ")";

	for (int x = min.x; x < max.x; x++)
		for (int y = min.y; y < max.y; y++)
			for (int z = min.z; z < max.z; z++)
				if (_node->at(ivec3(x, y, z)) > 0)
				{
					if (x < hl - 1 && _node->at(x + 1, y, z) == 0)
						generate_face<Face::West>(ivec3(x, y, z), _node->origin(), data);
					if (x > -hl && _node->at(x - 1, y, z) == 0)
						generate_face<Face::East>(ivec3(x, y, z), _node->origin(), data);

					if (y < hl - 1 && _node->at(x, y + 1, z) == 0)
						generate_face<Face::Up>(ivec3(x, y, z), _node->origin(), data);
					if (y > -hl && _node->at(x, y - 1, z) == 0)
						generate_face<Face::Down>(ivec3(x, y, z), _node->origin(), data);

					if (z < hl - 1 && _node->at(x, y, z + 1) == 0)
						generate_face<Face::North>(ivec3(x, y, z), _node->origin(), data);
					if (z > -hl && _node->at(x, y, z - 1) == 0)
						generate_face<Face::South>(ivec3(x, y, z), _node->origin(), data);
				}

	return data;
}
