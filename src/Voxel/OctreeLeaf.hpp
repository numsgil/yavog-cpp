#ifndef YAVOG_CPP_OCTREELEAF_HPP
#define YAVOG_CPP_OCTREELEAF_HPP

#include <array>

#include "OctreeNode.hpp"

constexpr glm::uint32 LeafSize = 16;
constexpr glm::uint32 HalfLeafSize = LeafSize / 2;

static inline int IndexAt(glm::ivec3 const& index)
{
	return index.x + (index.y * LeafSize) + (index.z * LeafSize * LeafSize);
}

static inline int IndexAt(int X, int Y, int Z)
{
	return IndexAt(glm::ivec3(X, Y, Z));
}

class OctreeLeaf : public OctreeNode
{
public:
	OctreeLeaf(glm::ivec3 const& origin);

	virtual ~OctreeLeaf();

	OctreeLeaf(OctreeLeaf const& other) = delete;

	OctreeLeaf(OctreeLeaf&& other) = delete;

	OctreeLeaf& operator=(OctreeLeaf const& other) = delete;

	OctreeLeaf& operator=(OctreeLeaf&& other) = delete;

	virtual bool is_leaf() const override
	{ return true; }

	virtual bool is_dirty() const override
	{ return _dirty; }

	virtual bool is_empty() const override
	{ return _empty; }

	virtual glm::uint16 at(glm::ivec3 const& position) const override;

	virtual void at(glm::ivec3 const& position, glm::uint16 value) override;

	virtual void traverse(std::function<void(OctreeNode*)> const& fn) override;

protected:
	std::array<glm::uint16, LeafSize * LeafSize * LeafSize> _data;
	bool _dirty, _empty;
};

#endif //YAVOG_CPP_OCTREELEAF_HPP
