#include <glm/gtc/matrix_transform.hpp>

#include "RenderingSystem.hpp"

#include "../Engine.hpp"

using namespace glm;

void Camera::update()
{
	if (pitch > 89.f) pitch = 89.f;
	if (pitch < -89.f) pitch = -89.f;

	front = normalize(vec3(cos(radians(yaw)) * cos(radians(pitch)), sin(radians(pitch)), sin(radians(yaw)) * cos(radians(pitch))));
	right = normalize(cross(front, vec3(0, 1, 0)));
	up = normalize(cross(right, front));
}

glm::mat4 Camera::to_mat4() const
{
	return lookAt(position, position + front, up);
}

vec3 Sun::direction() const
{
	vec4 tmp = mat4_cast(rotation) * vec4(0, 0, 1, 0);
	return {tmp.x, tmp.y, tmp.z};
}

RenderingSystem::RenderingSystem() : _proj(1.f)
{
}

RenderingSystem::~RenderingSystem()
{
}

void RenderingSystem::process()
{
	auto list = getEntities();

	mat4 view = _cam.to_mat4();
	vec3 light = _sun.direction();

	for (auto e : list)
	{
		auto mesh = e.getComponent<Renderable>();
		if (mesh.vertex_array().id() != 0 && mesh.vertex_buffer().id() != 0)
		{
			Engine::instance().renderer().use(mesh.shader());
			Engine::instance().renderer().use(mesh.vertex_array());
			mesh.shader().uniform("matModel").set(e.getComponent<Transform>().calculate());
			mesh.shader().uniform("matView").set(view);
			mesh.shader().uniform("matProj").set(_proj);

			mesh.shader().uniform("Light.direction").set(light);
			mesh.shader().uniform("Light.ambient").set(_sun.ambient);
			mesh.shader().uniform("Light.diffuse").set(_sun.diffuse);
			mesh.shader().uniform("Light.specular").set(_sun.specular);

			Engine::instance().renderer().use(mesh.material().texture, 0);
			mesh.shader().uniform("Material.diffuse").set(0);
			mesh.shader().uniform("Material.specular").set(mesh.material().specular);
			mesh.shader().uniform("Material.shininess").set(mesh.material().shininess);

			mesh.shader().uniform("vView").set(_cam.position);

			if (mesh.indexed())
				Engine::instance().renderer().draw(mesh.mode(), mesh.index_count(), mesh.index_type());
			else
				Engine::instance().renderer().draw(mesh.mode(), mesh.start(), mesh.count());
		}
	}
}
