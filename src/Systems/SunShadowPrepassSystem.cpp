#include <glm/gtc/matrix_transform.hpp>

#include "SunShadowPrepassSystem.hpp"
#include "RenderingSystem.hpp"
#include "../Engine.hpp"

using namespace glm;

SunShadowPrepassSystem::SunShadowPrepassSystem(int resolution) : _target(true, true), _depth(TextureTarget::Texture2D), _res(resolution, resolution), _prog()
{
	_depth.create();
	Engine::instance().renderer().use(_depth, 1);
	_depth.empty(_res, InternalFormat::Depth32, Format::Depth);

	_target.create();
	Engine::instance().renderer().framebuffer(_target);
	_target.attach_depth(_depth);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	CHECK(_target.status() == FramebufferStatus::Complete) << "Shadow target isn't Complete";
	Engine::instance().renderer().framebuffer();

	_prog.create();
	_prog.compile("ShadowPrepass");
	_prog.link();
}

SunShadowPrepassSystem::~SunShadowPrepassSystem()
{
}

void SunShadowPrepassSystem::process()
{
	auto list = getEntities();
	Sun& sun = Engine::instance().world().system<RenderingSystem>().sun();

	Engine::instance().renderer().framebuffer(_target);
	Engine::instance().renderer().clear(false, true, false);
	glViewport(0, 0, _res.x, _res.y);

	Engine::instance().renderer().use(_prog);
	_prog.uniform("matProj").set(ortho(-10.f, 10.f, -10.f, 10.f, 1.f, 1024.f));

	vec3 euler = eulerAngles(sun.rotation);
	vec3 front = normalize(vec3(cos(radians(euler.z)) * cos(radians(euler.x)), sin(radians(euler.x)), sin(radians(euler.z)) * cos(radians(euler.x))));
	vec3 right = normalize(cross(front, vec3(0, 1, 0)));
	vec3 up = normalize(cross(right, front));
	vec3 position = front * -100.f;
	_prog.uniform("matView").set(lookAt(position, position + front, up));

	for (auto e : list)
	{
		auto mesh = e.getComponent<Renderable>();
		if (mesh.vertex_array().id() != 0 && mesh.vertex_buffer().id() != 0)
		{
			Engine::instance().renderer().use(mesh.vertex_array());
			_prog.uniform("matModel").set(e.getComponent<Transform>().calculate());

			if (mesh.indexed())
				Engine::instance().renderer().draw(mesh.mode(), mesh.index_count(), mesh.index_type());
			else
				Engine::instance().renderer().draw(mesh.mode(), mesh.start(), mesh.count());
		}
	}

	Engine::instance().renderer().framebuffer();
	ivec2 fb = Engine::instance().framebuffer_size();
	glViewport(0, 0, fb.x, fb.y);
}
