#ifndef YAVOG_CPP_RENDERINGSYSTEM_HPP
#define YAVOG_CPP_RENDERINGSYSTEM_HPP

#include <anax/System.hpp>

#include "../Components/Renderable.hpp"
#include "../Components/Transform.hpp"

struct Camera
{
	Camera() : position(0, 0, 0), front(0, 0, -1), right(), up(0, 1, 0), yaw(-90.f), pitch(0)
	{ }

	void update();

	glm::mat4 to_mat4() const;

	glm::vec3 position, front, right, up;
	float yaw, pitch;
};

struct Sun
{
	Sun() : rotation(), ambient(0.2f, 0.2f, 0.2f), diffuse(0.8f, 0.8f, 0.8f), specular(1.f, 1.f, 1.f)
	{ }

	glm::vec3 direction() const;

	glm::quat rotation;
	glm::vec3 ambient, diffuse, specular;
};

class RenderingSystem : public anax::System<anax::Requires<Transform, Renderable>>
{
public:
	RenderingSystem();

	virtual ~RenderingSystem();

	RenderingSystem(RenderingSystem const& other) = delete;

	RenderingSystem(RenderingSystem&& other) = delete;

	RenderingSystem& operator=(RenderingSystem const& other) = delete;

	RenderingSystem& operator=(RenderingSystem&& other) = delete;

	virtual void projection(glm::mat4 const& proj)
	{ _proj = proj; }

	virtual glm::mat4 projection() const
	{ return _proj; }

	virtual Sun& sun()
	{ return _sun; }

	virtual Camera& camera()
	{ return _cam; }

	virtual void process();

protected:
	glm::mat4 _proj;
	Camera _cam;
	Sun _sun;
};

#endif //YAVOG_CPP_RENDERINGSYSTEM_HPP
