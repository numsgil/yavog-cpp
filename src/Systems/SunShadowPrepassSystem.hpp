#ifndef YAVOG_CPP_SUNSHADOWPREPASSSYSTEM_HPP
#define YAVOG_CPP_SUNSHADOWPREPASSSYSTEM_HPP

#include <anax/System.hpp>

#include "../Resources/Framebuffer.hpp"
#include "../Components/Renderable.hpp"
#include "../Components/Transform.hpp"

class SunShadowPrepassSystem : public anax::System<anax::Requires<Transform, Renderable>>
{
public:
	SunShadowPrepassSystem(int resolution);

	virtual ~SunShadowPrepassSystem();

	SunShadowPrepassSystem(SunShadowPrepassSystem const& other) = delete;

	SunShadowPrepassSystem(SunShadowPrepassSystem&& other) = delete;

	SunShadowPrepassSystem& operator=(SunShadowPrepassSystem const& other) = delete;

	SunShadowPrepassSystem& operator=(SunShadowPrepassSystem&& other) = delete;

	Texture depth_texture() const
	{ return _depth; }

	virtual void process();

protected:
	Shader _prog;
	Framebuffer _target;
	Texture _depth;
	glm::ivec2 _res;
};

#endif //YAVOG_CPP_SUNSHADOWPREPASSSYSTEM_HPP
