#include <glm/gtc/matrix_transform.hpp>

#include "Systems/SunShadowPrepassSystem.hpp"
#include "Systems/RenderingSystem.hpp"
#include "Voxel/SurfaceExtractor.hpp"
#include "Voxel/OctreeBranch.hpp"
#include "Utils/Perlin.hpp"
#include "Engine.hpp"

using namespace glm;
using namespace std;

inline vec3 to_rgb(int color)
{
	return vec3(
			((color >> 16) & 0xFF) / 255.f,
			((color >> 8) & 0xFF) / 255.f,
			((color) & 0xFF) / 255.f
	);
}

#ifdef WIN32

int WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
#else

int main(int argc, char** argv)
#endif
{
	Engine e;
	e.create();

	e.world().add_system<RenderingSystem>();
	e.world().system<RenderingSystem>().projection(perspective(45.f, (float) e.framebuffer_size().x / (float) e.framebuffer_size().y, 0.1f, 1024.f));
	e.world().system<RenderingSystem>().camera().position = vec3(0, 20, 0);
	e.world().system<RenderingSystem>().sun().rotation = angleAxis(pi<float>() / 4.f, vec3(1, 0, 0)) * angleAxis(-pi<float>() / 8.f, vec3(0, 1, 0));
	e.world().add_system<SunShadowPrepassSystem>(1024);

	Shader shader;
	shader.create();
	shader.compile("Basic");
	shader.link();

	Sampler s0;
	s0.create();
	s0.min_filter(MinFilter::Nearest);
	s0.mag_filter(MagFilter::Nearest);
	s0.wrap_s(Wrap::Repeat);
	s0.wrap_t(Wrap::Repeat);
	e.renderer().use(s0, 0);

	Texture stone(TextureTarget::Texture2D);
	stone.create();
	e.renderer().use(stone);
	stone.load_file("data/texture/stone.png");
	stone.generate_mipmaps();

	OctreeBranch node(ivec3(0, 0, 0), 128);
	SurfaceExtractor extractor(&node);
	Perlin p(1);
	for (int x = -(int) node.half_length(); x < (int) node.half_length(); x++)
		for (int z = -(int) node.half_length(); z < (int) node.half_length(); z++)
		{
			long h = lround(p.noise(x / 90.0, 0, z / 90.0) * 60);
			for (int y = -(int) node.half_length(); y < (int) node.half_length() && y < (int) h; y++)
				node.at(ivec3(x, y, z), 1);
		}

	auto chunk = e.world().createEntity();
	chunk.addComponent<Transform>();
	chunk.addComponent<Renderable>(&shader)
			.set_data(extractor.extract(Region(ivec3(-256, -256, -256), ivec3(256, 256, 256))), BufferUsage::StaticDraw)
			.bind_buffer_attribute(0, AttributeType::Float, 3, sizeof(SurfaceVertex), 0)                           // Position
			.bind_buffer_attribute(1, AttributeType::Float, 3, sizeof(SurfaceVertex), sizeof(vec3))                // Normal
			.bind_buffer_attribute(2, AttributeType::Float, 2, sizeof(SurfaceVertex), sizeof(vec3) + sizeof(vec3)) // Texture
			.mode(PrimitiveMode::Triangles)
			.material(stone, vec3(0.5f, 0.5f, 0.5f), 8.f);
	chunk.activate();

	/*Shader ui_shadow;
	ui_shadow.create();
	ui_shadow.compile("ShadowBuffer");
	ui_shadow.link();

	struct Quad_t
	{
		vec2 position, uv;
	};

	Buffer quad_vbo(BufferTarget::Array);
	quad_vbo.create();
	quad_vbo.set_data(array<Quad_t, 6>{{
			                                   {vec2(-1, 1), vec2(0, 1)},
			                                   {vec2(-1, -1), vec2(0, 0)},
			                                   {vec2(1, -1), vec2(1, 0)},

			                                   {vec2(-1, 1), vec2(0, 1)},
			                                   {vec2(1, -1), vec2(1, 0)},
			                                   {vec2(1, 1), vec2(1, 1)}
	                                   }}, BufferUsage::StaticDraw);
	VertexArray quad_vao;
	quad_vao.create();
	quad_vao.bind_buffer_attribute(quad_vbo, 0, AttributeType::Float, 2, sizeof(Quad_t), 0);
	quad_vao.bind_buffer_attribute(quad_vbo, 1, AttributeType::Float, 2, sizeof(Quad_t), sizeof(vec2));*/

	float movement_speed = 19.f;
	float delta_time, old_time = 0.f;
	bool looking = false;
	vec2 old_mouse;
	while (e.is_running())
	{
		{
			float current = e.time();
			delta_time = current - old_time;
			old_time = current;
		}

		if (e.is_key_down(Key::Escape))
			e.exit();

		if (e.is_mouse_down(Mouse::Right) && !looking)
		{
			e.mouse_mode(MouseMode::Disabled);
			looking = true;
		}
		if (!e.is_mouse_down(Mouse::Right) && looking)
		{
			e.mouse_mode(MouseMode::Normal);
			looking = false;
		}

		if (looking)
		{
			Camera& cam = e.world().system<RenderingSystem>().camera();
			vec2 delta = old_mouse - e.mouse_position();
			delta *= (float) e.settings().mouse_sensitivity() / 100.f;
			e.settings().mouse_inverse_y() ? (cam.yaw += delta.x) : (cam.yaw -= delta.x);
			cam.pitch += delta.y;
			cam.update();

			if (e.is_key_down(Key::W))
				cam.position += cam.front * movement_speed * delta_time;
			if (e.is_key_down(Key::S))
				cam.position -= cam.front * movement_speed * delta_time;
			if (e.is_key_down(Key::D))
				cam.position += cam.right * movement_speed * delta_time;
			if (e.is_key_down(Key::A))
				cam.position -= cam.right * movement_speed * delta_time;
			if (e.is_key_down(Key::E))
				cam.position += vec3(0, movement_speed * delta_time, 0);
			if (e.is_key_down(Key::Q))
				cam.position -= vec3(0, movement_speed * delta_time, 0);
		}

		//e.world().system<SunShadowPrepassSystem>().process();

		e.renderer().clear();
		/*e.renderer().use(ui_shadow);
		e.renderer().use(e.world().system<SunShadowPrepassSystem>().depth_texture(), 0);
		ui_shadow.uniform("tDepth").set(0);
		e.renderer().use(quad_vao);
		e.renderer().draw(PrimitiveMode::Triangles, 0, 6);*/
		e.world().system<RenderingSystem>().process();
		e.swap_buffers();

		old_mouse = e.mouse_position();
	}

	return 0;
}
