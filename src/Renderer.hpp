#ifndef YAVOG_CPP_RENDERER_HPP
#define YAVOG_CPP_RENDERER_HPP

#include "Resources/Framebuffer.hpp"
#include "Resources/VertexArray.hpp"
#include "Resources/Texture.hpp"
#include "Resources/Sampler.hpp"
#include "Resources/Buffer.hpp"
#include "Resources/Shader.hpp"

enum class PrimitiveMode : glm::uint32
{
	Points = 0x0000,
	LineStrip = 0x0003,
	LineLoop = 0x0002,
	Lines = 0x0001,
	LineStripAdjacency = 0x000B,
	LinesAdjacency = 0x000A,
	TriangleStrip = 0x0005,
	TriangleFan = 0x0006,
	Triangles = 0x0004,
	TriangleStripAdjacency = 0x000D,
	TrianglesAdjacency = 0x000C
};

enum class IndexType : glm::uint32
{
	UnsignedByte = 0x1401,
	UnsignedShort = 0x1403,
	UnsignedInteger = 0x1405
};

class Renderer
{
	friend class Engine;

	Renderer();

public:
	virtual ~Renderer();

	Renderer(Renderer const& other) = delete;

	Renderer(Renderer&& other) = delete;

	Renderer& operator=(Renderer const& other) = delete;

	Renderer& operator=(Renderer&& other) = delete;

	virtual void clear(bool color = true, bool depth = true, bool stencil = true);

	virtual void framebuffer(Framebuffer const& fbo);

	virtual void framebuffer();

	virtual void use(Buffer const& buf);

	virtual void use(Shader const& prog);

	virtual void use(Texture const& tex, glm::uint32 unit = 0);

	virtual void use(Sampler const& sam, glm::uint32 unit);

	virtual void use(VertexArray const& vao);

	virtual void draw(PrimitiveMode const& mode, glm::uint32 index, glm::uint32 count);

	virtual void draw(PrimitiveMode const& mode, glm::uint32 count, IndexType const& type);

protected:
	glm::uint32 _cur_prog, _cur_vao, _cur_read_fbo, _cur_write_fbo;
};

#endif //YAVOG_CPP_RENDERER_HPP
