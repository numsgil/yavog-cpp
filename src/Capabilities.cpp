#include <glbinding/gl40core/gl.h>
#include <g3log/g3log.hpp>

#include "Capabilities.hpp"

using namespace gl40core;

Capabilities::Capabilities()
{ }

void Capabilities::log_all() const
{
	LOG(INFO) << "Max Combined Texture Image Units : "
	          << max_combined_texture_image_units();
	LOG(INFO) << "Max CubeMap Texture Size : "
	          << max_cubemap_texture_size();
	LOG(INFO) << "Max Draw Buffers : "
	          << max_draw_buffers();
	LOG(INFO) << "Max Fragment Uniform Components : "
	          << max_fragment_uniform_components();
	LOG(INFO) << "Max Texture Image Units : "
	          << max_texture_image_units();
	LOG(INFO) << "Max Texture Size : "
	          << max_texture_size();
	LOG(INFO) << "Max Varying Floats : "
	          << max_varying_floats();
	LOG(INFO) << "Max Vertex Attributes : "
	          << max_vertex_attributes();
	LOG(INFO) << "Max Vertex Texture Image Units : "
	          << max_vertex_texture_image_units();
	LOG(INFO) << "Max Vertex Uniform Components : "
	          << max_vertex_uniform_components();
	auto viewport_dims = max_viewport_dimensions();
	LOG(INFO) << "Max Viewport Dimensions : "
	          << viewport_dims.x << ", " << viewport_dims.y;
	LOG(INFO) << "Max Uniform Block Size : "
	          << max_uniform_block_size();
	LOG(INFO) << "Max Texture Buffer Size : "
	          << max_texture_buffer_size();
	LOG(INFO) << "Max Rectangle Texture Size : "
	          << max_rectangle_texture_size();
	LOG(INFO) << "Max Array Texture Layers : "
	          << max_array_texture_layers();
}

int Capabilities::max_combined_texture_image_units() const
{
	int value = 0;
	glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &value);
	return value;
}

int Capabilities::max_cubemap_texture_size() const
{
	int value = 0;
	glGetIntegerv(GL_MAX_CUBE_MAP_TEXTURE_SIZE, &value);
	return value;
}

int Capabilities::max_draw_buffers() const
{
	int value = 0;
	glGetIntegerv(GL_MAX_DRAW_BUFFERS, &value);
	return value;
}

int Capabilities::max_fragment_uniform_components() const
{
	int value = 0;
	glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_COMPONENTS, &value);
	return value;
}

int Capabilities::max_texture_image_units() const
{
	int value = 0;
	glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &value);
	return value;
}

int Capabilities::max_texture_size() const
{
	int value = 0;
	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &value);
	return value;
}

int Capabilities::max_varying_floats() const
{
	int value = 0;
	glGetIntegerv(GL_MAX_VARYING_FLOATS, &value);
	return value;
}

int Capabilities::max_vertex_attributes() const
{
	int value = 0;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &value);
	return value;
}

int Capabilities::max_vertex_texture_image_units() const
{
	int value = 0;
	glGetIntegerv(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, &value);
	return value;
}

int Capabilities::max_vertex_uniform_components() const
{
	int value = 0;
	glGetIntegerv(GL_MAX_VERTEX_UNIFORM_COMPONENTS, &value);
	return value;
}

glm::ivec2 Capabilities::max_viewport_dimensions() const
{
	int value[2];
	glGetIntegerv(GL_MAX_VIEWPORT_DIMS, value);
	return glm::ivec2(value[0], value[1]);
}

int Capabilities::max_uniform_block_size() const
{
	int value = 0;
	glGetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, &value);
	return value;
}

int Capabilities::max_texture_buffer_size() const
{
	int value = 0;
	glGetIntegerv(GL_MAX_TEXTURE_BUFFER_SIZE, &value);
	return value;
}

int Capabilities::max_rectangle_texture_size() const
{
	int value = 0;
	glGetIntegerv(GL_MAX_RECTANGLE_TEXTURE_SIZE, &value);
	return value;
}

int Capabilities::max_array_texture_layers() const
{
	int value = 0;
	glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &value);
	return value;
}
