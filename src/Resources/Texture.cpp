#define STB_IMAGE_IMPLEMENTATION

#include <g3log/g3log.hpp>
#include <stb_image.h>

#include "Texture.hpp"

using namespace glm;
using namespace std;

Texture::Texture(TextureTarget const& target) : _target(target), _size(), _comps(TextureComponents::RGBA)
{
}

Texture::~Texture()
{
}

Texture::Texture(Texture const& other) : _target(other._target), _size(other._size), _comps(other._comps)
{
	_id = other._id;
}

Texture::Texture(Texture&& other) : _target(other._target), _size(other._size), _comps(other._comps)
{
	_id = other._id;

	other._id = 0;
	other._size = ivec3();
	other._comps = TextureComponents::RGBA;
}

Texture& Texture::operator=(Texture const& other)
{
	_id = other._id;
	_target = other._target;
	_size = other._size;
	_comps = other._comps;

	return *this;
}

Texture& Texture::operator=(Texture&& other)
{
	_id = other._id;
	_target = other._target;
	_size = other._size;
	_comps = other._comps;

	other._id = 0;
	other._size = ivec3();
	other._comps = TextureComponents::RGBA;

	return *this;
}

void Texture::create()
{
	if (_id != 0) destroy();
	glGenTextures(1, &_id);

	string target = "";
	switch (_target)
	{
		case TextureTarget::Texture1D:
			target = "Texture1D";
			break;
		case TextureTarget::Texture2D:
			target = "Texture2D";
			break;
		case TextureTarget::Texture3D:
			target = "Texture3D";
			break;
		case TextureTarget::Texture1DArray:
			target = "Texture1DArray";
			break;
		case TextureTarget::Texture2DArray:
			target = "Texture2DArray";
			break;
		case TextureTarget::CubeMap:
			target = "CubeMap";
			break;
		case TextureTarget::CubeMapArray:
			target = "CubeMapArray";
			break;
		case TextureTarget::Multisample2D:
			target = "Multisample2D";
			break;
		case TextureTarget::Multisample2DArray:
			target = "Multisample2DArray";
			break;
		case TextureTarget::Rectangle:
			target = "Rectangle";
			break;
	}

	LOG(INFO) << "Texture created with ID " << _id << " (" << target << ")";
}

void Texture::destroy()
{
	if (_id == 0) return;
	glDeleteTextures(1, &_id);
	_id = 0;
}

void Texture::generate_mipmaps()
{
	glGenerateMipmap((GLenum) _target);
}

void Texture::load_file(string const& filename, int components_requested, Format const& format)
{
	CHECK(_target == TextureTarget::Texture2D || _target == TextureTarget::Rectangle) << "Only loading from 2D Textures is supported";
	unsigned char* data = stbi_load(filename.c_str(), &_size.x, &_size.y, (int*) &_comps, components_requested);
	CHECK(data != nullptr) << "Could not load Texture : " << filename;

	glTexImage2D((GLenum) _target, 0, (GLint) GL_RGBA, _size.x, _size.y, 0, (GLenum) format, GL_UNSIGNED_BYTE, data);
	stbi_image_free(data);
	_size.z = 0;
	LOG(INFO) << "Texture with ID " << _id << " loaded from file : " << filename << " (" << _size.x << ", " << _size.y << ")";
}

void Texture::empty(glm::ivec2 const& size, InternalFormat const& internal, Format const& format)
{
	_size = ivec3(size, 0);
	glTexImage2D((GLenum) _target, 0, (int) internal, _size.x, _size.y, 0, (GLenum) format,
	             internal == InternalFormat::Depth24Stencil8 ? GL_UNSIGNED_INT_24_8 : GL_UNSIGNED_BYTE,
	             0);
	LOG(INFO) << "Texture with ID " << _id << " emptied (" << _size.x << ", " << _size.y << ")";
}

void Texture::empty(glm::ivec3 const& size, InternalFormat const& internal, Format const& format)
{
	_size = size;
	glTexImage3D((GLenum) _target, 0, (int) internal, _size.x, _size.y, _size.z, 0, (GLenum) format, GL_UNSIGNED_BYTE, 0);
	LOG(INFO) << "Texture with ID " << _id << " emptied (" << _size.x << ", " << _size.y << ", " << _size.z << ")";
}
