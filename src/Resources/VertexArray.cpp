#include <g3log/g3log.hpp>

#include "VertexArray.hpp"
#include "../Engine.hpp"

using namespace glm;

VertexArray::VertexArray()
{
}

VertexArray::~VertexArray()
{
}

void VertexArray::create()
{
	if (_id != 0) destroy();
	glGenVertexArrays(1, &_id);
	LOG(INFO) << "Vertex Array created with ID " << _id;
}

void VertexArray::destroy()
{
	if (_id == 0) return;
	glDeleteVertexArrays(1, &_id);
	_id = 0;
}

void VertexArray::bind_buffer_attribute(Buffer& buffer, uint32 index, AttributeType const& type, int size, uint32 stride, uint32 offset, bool normalized)
{
	Engine::instance().renderer().use(*this);
	glEnableVertexAttribArray(index);
	Engine::instance().renderer().use(buffer);
	glVertexAttribPointer(index, size, (GLenum) type, (GLboolean) (normalized ? GL_TRUE : GL_FALSE), stride, reinterpret_cast<void*>(offset));
}

void VertexArray::bind_index_buffer(Buffer& buffer)
{
	CHECK(buffer.target() == BufferTarget::ElementArray) << "Buffer is not an index buffer";
	Engine::instance().renderer().use(*this);
	Engine::instance().renderer().use(buffer);
}
