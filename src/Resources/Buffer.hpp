#ifndef YAVOG_CPP_BUFFER_HPP
#define YAVOG_CPP_BUFFER_HPP

#include <vector>
#include <array>

#include "Resource.hpp"

enum class BufferTarget : glm::uint32
{
	Array = 0x8892,
	ElementArray = 0x8893,
	CopyRead = 0x8F36,
	CopyWrite = 0x8F37,
	PixelPack = 0x88EB,
	PixelUnpack = 0x88EC,
	Query = 0x9192,
	ShaderStorage = 0x90D2,
	Texture = 0x8C2A,
	TransformFeedback = 0x8C8E,
	Uniform = 0x8A11
};

enum class BufferUsage : glm::uint32
{
	StaticDraw = 0x88E4,
	StaticRead = 0x88E5,
	StaticCopy = 0x88E6,
	StreamDraw = 0x88E0,
	StreamRead = 0x88E1,
	StreamCopy = 0x88E2,
	DynamicDraw = 0x88E8,
	DynamicRead = 0x88E9,
	DynamicCopy = 0x88EA
};

class Buffer : public Resource<glm::uint32>
{
public:
	Buffer(BufferTarget const& target);

	virtual ~Buffer();

	Buffer(Buffer const& other);

	Buffer(Buffer&& other);

	Buffer& operator=(Buffer const& other);

	Buffer& operator=(Buffer&& other);

	virtual BufferTarget target() const
	{ return _target; }

	virtual void create() override;

	virtual void destroy() override;

	virtual void set_data(const void* ptr, std::size_t size, BufferUsage const& usage);

	template<typename T>
	void set_data(const std::vector<T>& data, BufferUsage usage);

	template<typename T, std::size_t Size>
	void set_data(const std::array<T, Size>& data, BufferUsage usage);

protected:
	BufferTarget _target;
};

template<typename T>
inline void Buffer::set_data(const std::vector<T>& data, BufferUsage usage)
{
	set_data(&data[0], (std::size_t) sizeof(T) * data.size(), usage);
}

template<typename T, std::size_t Size>
inline void Buffer::set_data(const std::array<T, Size>& data, BufferUsage usage)
{
	set_data(&data[0], (std::size_t) sizeof(T) * Size, usage);
}

#endif //YAVOG_CPP_BUFFER_HPP
