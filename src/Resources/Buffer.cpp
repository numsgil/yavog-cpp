#include <g3log/g3log.hpp>

#include "Buffer.hpp"

Buffer::Buffer(BufferTarget const& target) : _target(target)
{
}

Buffer::~Buffer()
{
}

Buffer::Buffer(Buffer const& other) : _target(other._target)
{
	_id = other._id;
}

Buffer::Buffer(Buffer&& other) : _target(other._target)
{
	_id = other._id;
	other._id = 0;
	other._target = BufferTarget();
}

Buffer& Buffer::operator=(Buffer const& other)
{
	_id = other._id;
	_target = other._target;
	return *this;
}

Buffer& Buffer::operator=(Buffer&& other)
{
	_id = other._id;
	_target = other._target;
	other._id = 0;
	other._target = BufferTarget();
	return *this;
}

void Buffer::create()
{
	if (_id != 0) destroy();
	glGenBuffers(1, &_id);

	std::string target = "";
	switch (_target)
	{
		case BufferTarget::Array:
			target = "Array";
			break;
		case BufferTarget::ElementArray:
			target = "ElementArray";
			break;
		case BufferTarget::CopyRead:
			target = "CopyRead";
			break;
		case BufferTarget::CopyWrite:
			target = "CopyWrite";
			break;
		case BufferTarget::PixelPack:
			target = "PixelPack";
			break;
		case BufferTarget::PixelUnpack:
			target = "PixelUnpack";
			break;
		case BufferTarget::Query:
			target = "Query";
			break;
		case BufferTarget::ShaderStorage:
			target = "ShaderStorage";
			break;
		case BufferTarget::Texture:
			target = "Texture";
			break;
		case BufferTarget::TransformFeedback:
			target = "TransformFeedback";
			break;
		case BufferTarget::Uniform:
			target = "Uniform";
			break;
	}

	LOG(INFO) << "Buffer created with ID " << _id << ", target : " << target;
}

void Buffer::destroy()
{
	if (_id == 0) return;
	glDeleteBuffers(1, &_id);
	_id = 0;
}

void Buffer::set_data(const void* ptr, std::size_t size, BufferUsage const& usage)
{
	glBindBuffer((GLenum) _target, _id);
	glBufferData((GLenum) _target, size, ptr, (GLenum) usage);
	glBindBuffer((GLenum) _target, 0);
}
