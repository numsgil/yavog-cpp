#include "Shader.hpp"

#include <glm/gtc/type_ptr.hpp>
#include <g3log/g3log.hpp>
#include <fstream>
#include <regex>

using namespace std;
using namespace glm;

string Shader::ShaderDirectory = "data/shader/";

ShaderUniform::ShaderUniform() : _size(0), _location(0), _type(), _name()
{
}

ShaderUniform::ShaderUniform(ShaderUniform const& other)
		: _size(other._size), _location(other._location), _type(other._type), _name(other._name), _prog(other._prog)
{
	if (_location == 0 && !_name.empty())
		_location = glGetUniformLocation(_prog, _name.c_str());
}

ShaderUniform::ShaderUniform(ShaderUniform&& other)
		: _size(other._size), _location(other._location), _type(other._type), _name(other._name), _prog(other._prog)
{
	if (_location == 0 && !_name.empty())
		_location = glGetUniformLocation(_prog, _name.c_str());

	other._size = 0;
	other._location = 0;
	other._type = UniformType();
	other._name.clear();
	other._prog = 0;
}

ShaderUniform& ShaderUniform::operator=(ShaderUniform const& other)
{
	_size = other._size;
	_location = other._location;
	_type = other._type;
	_name = other._name;
	_prog = other._prog;

	if (_location == 0 && !_name.empty())
		_location = glGetUniformLocation(_prog, _name.c_str());

	return *this;
}

ShaderUniform& ShaderUniform::operator=(ShaderUniform&& other)
{
	_size = other._size;
	_location = other._location;
	_type = other._type;
	_name = other._name;
	_prog = other._prog;

	if (_location == 0 && !_name.empty())
		_location = glGetUniformLocation(_prog, _name.c_str());

	other._size = 0;
	other._location = 0;
	other._type = UniformType();
	other._name.clear();
	other._prog = 0;

	return *this;
}

ShaderUniform::ShaderUniform(uint32 prog, int size, UniformType const& type, string const& name)
		: _prog(prog), _size(size), _location(0), _type(type), _name(name)
{
	_location = glGetUniformLocation(_prog, _name.c_str());
}

void ShaderUniform::set(int value)
{
	CHECK(_type == UniformType::Integer || _type == UniformType::Sampler1D
	      || _type == UniformType::Sampler1D
	      || _type == UniformType::Sampler2D
	      || _type == UniformType::Sampler3D
	      || _type == UniformType::Sampler1DArray
	      || _type == UniformType::Sampler2DArray) << "Uniform is not an Integer type";
	glUniform1i(_location, value);
}

void ShaderUniform::set(ivec2 const& value)
{
	CHECK(_type == UniformType::IntegerVector2) << "Uniform is not a IntegerVector2 type";
	glUniform2i(_location, value.x, value.y);
}

void ShaderUniform::set(ivec3 const& value)
{
	CHECK(_type == UniformType::IntegerVector3) << "Uniform is not a FloatVector3 type";
	glUniform3i(_location, value.x, value.y, value.z);
}

void ShaderUniform::set(ivec4 const& value)
{
	CHECK(_type == UniformType::IntegerVector4) << "Uniform is not a IntegerVector4 type";
	glUniform4i(_location, value.x, value.y, value.z, value.w);
}

void ShaderUniform::set(float value)
{
	CHECK(_type == UniformType::Float) << "Uniform is not a Float type";
	glUniform1f(_location, value);
}

void ShaderUniform::set(vec2 const& value)
{
	CHECK(_type == UniformType::FloatVector2) << "Uniform is not a FloatVector2 type";
	glUniform2f(_location, value.x, value.y);
}

void ShaderUniform::set(vec3 const& value)
{
	CHECK(_type == UniformType::FloatVector3) << "Uniform is not a FloatVector3 type";
	glUniform3f(_location, value.x, value.y, value.z);
}

void ShaderUniform::set(vec4 const& value)
{
	CHECK(_type == UniformType::FloatVector4) << "Uniform is not a FloatVector4 type";
	glUniform4f(_location, value.x, value.y, value.z, value.w);
}

void ShaderUniform::set(mat2 const& value)
{
	CHECK(_type == UniformType::FloatMatrix2) << "Uniform is not a FloatMatrix2 type";
	glUniformMatrix2fv(_location, 1, GL_FALSE, value_ptr(value));
}

void ShaderUniform::set(mat3 const& value)
{
	CHECK(_type == UniformType::FloatMatrix3) << "Uniform is not a FloatMatrix3 type";
	glUniformMatrix3fv(_location, 1, GL_FALSE, value_ptr(value));
}

void ShaderUniform::set(mat4 const& value)
{
	CHECK(_type == UniformType::FloatMatrix4) << "Uniform is not a FloatMatrix4 type";
	glUniformMatrix4fv(_location, 1, GL_FALSE, value_ptr(value));
}

Shader::Shader() : _tess_ctrl(false), _tess_eval(false), _geo(false)
{
}

Shader::~Shader()
{
}

Shader::Shader(Shader const& other) : _tess_ctrl(other._tess_ctrl), _tess_eval(other._tess_eval), _geo(other._geo), _uniforms(other._uniforms)
{
	_id = other._id;
}

Shader::Shader(Shader&& other) : _tess_ctrl(other._tess_ctrl), _tess_eval(other._tess_eval), _geo(other._geo), _uniforms(move(other._uniforms))
{
	_id = other._id;

	other._id = 0;
	other._tess_ctrl = false;
	other._tess_eval = false;
	other._geo = false;
}

Shader& Shader::operator=(Shader const& other)
{
	_id = other._id;
	_tess_ctrl = other._tess_ctrl;
	_tess_eval = other._tess_eval;
	_geo = other._geo;
	_uniforms = other._uniforms;

	return *this;
}

Shader& Shader::operator=(Shader&& other)
{
	_id = other._id;
	_tess_ctrl = other._tess_ctrl;
	_tess_eval = other._tess_eval;
	_geo = other._geo;
	_uniforms = move(other._uniforms);

	other._id = 0;
	other._tess_ctrl = false;
	other._tess_eval = false;
	other._geo = false;

	return *this;
}

void Shader::create()
{
	if (_id != 0) destroy();
	_id = glCreateProgram();
	LOG(INFO) << "Shader created with ID " << _id;
}

void Shader::destroy()
{
	if (_id == 0) return;
	glDeleteProgram(_id);
	_id = 0;
}

void Shader::compile(string const& filename)
{
	_tess_eval = false;
	_tess_ctrl = false;
	_geo = false;

	auto lines = load_file(filename);
	parse(lines);

	string file;
	for(auto l : lines) file += l + "\n";

	compile_shader(GL_VERTEX_SHADER, "#version 400 core\n#define VERTEX\n" + file);
	compile_shader(GL_FRAGMENT_SHADER, "#version 400 core\n#define FRAGMENT\n" + file);
	LOG(INFO) << "Shader with ID " << _id << " compiled successfully from " << ShaderDirectory + filename + ".glsl";
}

void Shader::link()
{
	glLinkProgram(_id);

	int link_ok = 0;
	glGetProgramiv(_id, GL_LINK_STATUS, &link_ok);
	if (!link_ok)
	{
		int length = 0;
		glGetProgramiv(_id, GL_INFO_LOG_LENGTH, &length);
		char* log_ = new char[length];
		glGetProgramInfoLog(_id, length, 0, log_);

		string log(log_);
		delete[] log_;
		LOG(FATAL) << "Shader Program linkage error : \n" << log;
	}

	LOG(INFO) << "Shader with ID " << _id << " linked successfully";

	if (!_uniforms.empty())
		_uniforms.clear();

	int count;
	glGetProgramiv(_id, GL_ACTIVE_UNIFORMS, &count);
	for (uint i = 0; i < count; i++)
	{
		int length, size;
		GLenum type;
		char name[512];
		glGetActiveUniform(_id, i, 512, &length, &size, &type, name);
		string str(name);
		_uniforms[name] = ShaderUniform(_id, size, (UniformType) type, str);
	}

	LOG(INFO) << "Shader with ID " << _id << " has " << count << " active uniforms";
}

bool Shader::has_uniform(string const& name) const
{
	return _uniforms.find(name) != _uniforms.end();
}

ShaderUniform& Shader::uniform(string const& name)
{
	CHECK(has_uniform(name)) << name << " is not an active uniform";
	return _uniforms.at(name);
}

vector<string> Shader::load_file(string const& filename) const
{
	ifstream file(ShaderDirectory + filename + ".glsl");
	CHECK(file.is_open()) << "Could not open file '" << ShaderDirectory + filename << ".glsl'";
	vector<string> ret;

	for(string line; getline(file, line); )
		ret.push_back(line);

	return ret;
}

inline string trim(string const& s)
{
	auto wsfront = find_if_not(s.begin(), s.end(), [](int c)
	{
		return isspace(c);
	});

	auto wsback = find_if_not(s.rbegin(), s.rend(), [](int c)
	{
		return isspace(c);
	}).base();

	return (wsback <= wsfront ? string() : string(wsfront, wsback));
}

void Shader::parse(vector<string>& lines) const
{
	bool finished = false;
	while (!finished)
	{
		int i;
		for (i = 0; i < lines.size(); i++)
		{
			string line(lines[i]);

			regex import("#pragma +import +(.*) *", regex_constants::extended);
			smatch match;
			if (regex_match(line, match, import))
			{
				if (match.size() == 2)
				{
					lines.erase(lines.begin() + i);
					auto file = load_file(trim(match[1].str().c_str()));
					lines.insert(lines.begin() + i, file.begin(), file.end());
					break;
				}
			}
		}

		if (i >= lines.size()) finished = true;
	}
}

void Shader::compile_shader(GLenum type, string const& file)
{
	uint32 s = glCreateShader(type);
	const char* data = file.data();
	glShaderSource(s, 1, &data, 0);
	glCompileShader(s);
	int compile_ok = 0;
	glGetShaderiv(s, GL_COMPILE_STATUS, &compile_ok);
	if (!compile_ok)
	{
		int length = 0;
		glGetShaderiv(s, GL_INFO_LOG_LENGTH, &length);
		char* log_ = new char[length];
		glGetShaderInfoLog(s, length, 0, log_);

		string log(log_);
		delete[] log_;
		LOG(FATAL) << "Shader compilation error : \n" << log;
	}
	glAttachShader(_id, s);
	glDeleteShader(s);
}
