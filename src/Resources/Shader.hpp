#ifndef YAVOG_CPP_SHADER_HPP
#define YAVOG_CPP_SHADER_HPP

#include <string>
#include <vector>
#include <map>

#include "Resource.hpp"

enum class UniformType : glm::uint32
{
	Integer = 0x1404,
	IntegerVector2 = 0x8B53,
	IntegerVector3 = 0x8B54,
	IntegerVector4 = 0x8B55,
	Float = 0x1406,
	FloatVector2 = 0x8B50,
	FloatVector3 = 0x8B51,
	FloatVector4 = 0x8B52,
	Sampler1D = 0x8B5D,
	Sampler2D = 0x8B5E,
	Sampler3D = 0x8B5F,
	Sampler1DArray = 0x8DC0,
	Sampler2DArray = 0x8DC1,
	FloatMatrix2 = 0x8B5A,
	FloatMatrix3 = 0x8B5B,
	FloatMatrix4 = 0x8B5C
};

class ShaderUniform
{
	friend class Shader;

public:
	ShaderUniform();

	ShaderUniform(ShaderUniform const& other);

	ShaderUniform(ShaderUniform&& other);

	ShaderUniform& operator=(ShaderUniform const& other);

	ShaderUniform& operator=(ShaderUniform&& other);

	void set(int value);

	void set(glm::ivec2 const& value);

	void set(glm::ivec3 const& value);

	void set(glm::ivec4 const& value);

	void set(float value);

	void set(glm::vec2 const& value);

	void set(glm::vec3 const& value);

	void set(glm::vec4 const& value);

	void set(glm::mat2 const& value);

	void set(glm::mat3 const& value);

	void set(glm::mat4 const& value);

protected:
	ShaderUniform(glm::uint32 prog, int size, UniformType const& type, std::string const& name);

	glm::uint32 _prog;
	int _size, _location;
	UniformType _type;
	std::string _name;
};

class Shader : public Resource<glm::uint32>
{
public:
	static std::string ShaderDirectory;

	Shader();

	virtual ~Shader();

	Shader(Shader const& other);

	Shader(Shader&& other);

	Shader& operator=(Shader const& other);

	Shader& operator=(Shader&& other);

	virtual void create() override;

	virtual void destroy() override;

	virtual void compile(std::string const& filename);

	virtual void link();

	virtual bool has_uniform(std::string const& name) const;

	virtual ShaderUniform& uniform(std::string const& name);

protected:
	std::vector<std::string> load_file(std::string const& filename) const;

	void parse(std::vector<std::string>& lines) const;

	void compile_shader(GLenum type, std::string const& file);

	std::map<std::string, ShaderUniform> _uniforms;
	bool _tess_eval, _tess_ctrl, _geo;
};

#endif //YAVOG_CPP_SHADER_HPP
