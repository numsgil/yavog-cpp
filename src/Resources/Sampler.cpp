#include <glm/gtc/type_ptr.hpp>
#include <g3log/g3log.hpp>

#include "Sampler.hpp"

Sampler::Sampler()
{
}

Sampler::~Sampler()
{
}

void Sampler::create()
{
	if (_id != 0) destroy();
	glGenSamplers(1, &_id);
	LOG(INFO) << "Sampler Object created with ID " << _id;
}

void Sampler::destroy()
{
	if (_id == 0) return;
	glDeleteSamplers(1, &_id);
	_id = 0;
}

void Sampler::min_filter(MinFilter const& filter)
{
	glSamplerParameteri(_id, GL_TEXTURE_MIN_FILTER, (int) filter);
}

void Sampler::mag_filter(MagFilter const& filter)
{
	glSamplerParameteri(_id, GL_TEXTURE_MAG_FILTER, (int) filter);
}

void Sampler::min_lod(float value)
{
	glSamplerParameterf(_id, GL_TEXTURE_MIN_LOD, value);
}

void Sampler::max_lod(float value)
{
	glSamplerParameterf(_id, GL_TEXTURE_MAX_LOD, value);
}

void Sampler::wrap_s(Wrap const& value)
{
	glSamplerParameteri(_id, GL_TEXTURE_WRAP_S, (int) value);
}

void Sampler::wrap_t(Wrap const& value)
{
	glSamplerParameteri(_id, GL_TEXTURE_WRAP_T, (int) value);
}

void Sampler::wrap_r(Wrap const& value)
{
	glSamplerParameteri(_id, GL_TEXTURE_WRAP_R, (int) value);
}

void Sampler::border_color(glm::vec4 const& color)
{
	glSamplerParameterfv(_id, GL_TEXTURE_BORDER_COLOR, glm::value_ptr(color));
}

void Sampler::compare_mode(CompareMode const& mode)
{
	glSamplerParameteri(_id, GL_TEXTURE_COMPARE_MODE, (int) mode);
}

void Sampler::compare_function(CompareFunction const& func)
{
	glSamplerParameteri(_id, GL_TEXTURE_COMPARE_FUNC, (int) func);
}
