#ifndef YAVOG_CPP_FRAMEBUFFER_HPP
#define YAVOG_CPP_FRAMEBUFFER_HPP

#include "Resource.hpp"
#include "Texture.hpp"

enum class FramebufferStatus : glm::uint32
{
	Complete = 0x8CD5,
	Undefined = 0x8219,
	IncompleteAttachment = 0x8CD6,
	MissingAttachment = 0x8CD7,
	IncompleteDrawBuffer = 0x8CDB,
	IncompleteReadBuffer = 0x8CDC,
	Unsupported = 0x8CDD,
	IncompleteMultisample = 0x8D56,
	IncompleteLayerTargets = 0x8DA8
};

class Framebuffer : public Resource<glm::uint32>
{
public:
	Framebuffer(bool read = true, bool write = true);

	virtual ~Framebuffer();

	Framebuffer(Framebuffer const& other) = default;

	Framebuffer(Framebuffer&& other) = default;

	Framebuffer& operator=(Framebuffer const& other) = default;

	Framebuffer& operator=(Framebuffer&& other) = default;

	virtual bool read_only() const
	{ return _read && !_write; }

	virtual bool write_only() const
	{ return !_read && _write; }

	virtual GLenum target() const;

	virtual void create();

	virtual void destroy();

	virtual FramebufferStatus status() const;

	virtual void attach_color(Texture const& texture, glm::uint32 attachment = 0);

	virtual void attach_depth(Texture const& texture);

protected:
	bool _read, _write;
};

#endif //YAVOG_CPP_FRAMEBUFFER_HPP
