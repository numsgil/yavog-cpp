#ifndef YAVOG_CPP_RESOURCE_HPP
#define YAVOG_CPP_RESOURCE_HPP

#include <glbinding/gl40core/gl.h>
#include <glm/glm.hpp>

using namespace gl40core;

template<typename T>
class Resource
{
public:
	Resource() : _id((T) 0)
	{
	}

	virtual ~Resource()
	{
		_id = (T) 0;
	}

	Resource(Resource<T> const& other) : _id(other._id)
	{
	}

	Resource(Resource<T>&& other) : _id(other._id)
	{
		other._id = (T) 0;
	}

	Resource<T>& operator=(Resource<T> const& other)
	{
		_id = other._id;
		return *this;
	}

	Resource& operator=(Resource&& other)
	{
		_id = other._id;
		other._id = (T) 0;
		return *this;
	}

	T id() const
	{ return _id; }

	virtual void create() = 0;

	virtual void destroy() = 0;

protected:
	T _id;
};

#endif //YAVOG_CPP_RESOURCE_HPP
