#ifndef YAVOG_CPP_TEXTURE_HPP
#define YAVOG_CPP_TEXTURE_HPP

#include <glm/glm.hpp>
#include <string>

#include "Resource.hpp"

enum class TextureTarget : glm::uint32
{
	Texture1D = 0x0DE0,
	Texture2D = 0x0DE1,
	Texture3D = 0x806F,
	Texture1DArray = 0x8C18,
	Texture2DArray = 0x8C1A,
	Rectangle = 0x84F5,
	CubeMap = 0x8513,
	CubeMapArray = 0x9009,
	Multisample2D = 0x9100,
	Multisample2DArray = 0x9102
};

enum class TextureComponents : int
{
	R = 1,
	RA = 2,
	RGB = 3,
	RGBA = 4
};

enum class Format : glm::uint32
{
	R = 0x1903,
	RG = 0x8227,
	RGB = 0x1907,
	BGR = 0x80E0,
	RGBA = 0x1908,
	BGRA = 0x80E1,
	Depth = 0x1902,
	DepthStencil = 0x84F9
};

enum class InternalFormat : int
{
	R = 0x1903,
	RG = 0x8227,
	RGB = 0x1907,
	BGR = 0x80E0,
	RGBA = 0x1908,
	BGRA = 0x80E1,
	Depth16 = 0x81A5,
	Depth24 = 0x81A6,
	Depth32 = 0x81A7,
	Depth24Stencil8 = 0x88F0,

	R16f = 0x822D,
	RG16f = 0x822F,
	RGB16f = 0x881B,
	RGBA16f = 0x881A,
	R32f = 0x822E,
	RG32f = 0x8230,
	RGB32f = 0x8815,
	RGBA32f = 0x8814
};

class Texture : public Resource<glm::uint32>
{
public:
	Texture(TextureTarget const& target);

	virtual ~Texture();

	Texture(Texture const& other);

	Texture(Texture&& other);

	Texture& operator=(Texture const& other);

	Texture& operator=(Texture&& other);

	virtual TextureTarget target() const
	{ return _target; }

	virtual TextureComponents components() const
	{ return _comps; }

	virtual glm::ivec3 size() const
	{ return _size; }

	virtual void create();

	virtual void destroy();

	virtual void generate_mipmaps();

	virtual void load_file(std::string const& filename, int components_requested = 4, Format const& format = Format::RGBA);

	virtual void empty(glm::ivec2 const& size, InternalFormat const& internal, Format const& format);

	virtual void empty(glm::ivec3 const& size, InternalFormat const& internal, Format const& format);

protected:
	TextureTarget _target;
	TextureComponents _comps;
	glm::ivec3 _size;
};

#endif //YAVOG_CPP_TEXTURE_HPP
