#ifndef YAVOG_CPP_VERTEXARRAY_HPP
#define YAVOG_CPP_VERTEXARRAY_HPP

#include "Buffer.hpp"

enum class AttributeType : glm::uint32
{
	Byte = 0x1400,
	UnsignedByte = 0x1401,
	Short = 0x1402,
	UnsignedShort = 0x1403,
	Integer = 0x1404,
	UnsignedInteger = 0x1405,
	Float = 0x1406,
	Double = 0x140A,
	HalfFloat = 0x140B
};

class VertexArray : public Resource<glm::uint32>
{
public:
	VertexArray();

	virtual ~VertexArray();

	VertexArray(VertexArray const& other) = default;

	VertexArray(VertexArray&& other) = default;

	VertexArray& operator=(VertexArray const& other) = default;

	VertexArray& operator=(VertexArray&& other) = default;

	virtual void create() override;

	virtual void destroy() override;

	void bind_buffer_attribute(Buffer& buffer, glm::uint32 index, AttributeType const& type, int size, glm::uint32 stride,
	                           glm::uint32 offset, bool normalized = false);

	void bind_index_buffer(Buffer& buffer);
};

#endif //YAVOG_CPP_VERTEXARRAY_HPP
