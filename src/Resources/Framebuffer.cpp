#include <g3log/g3log.hpp>

#include "Framebuffer.hpp"

Framebuffer::Framebuffer(bool read, bool write) : _read(read), _write(write)
{ }

Framebuffer::~Framebuffer()
{ }

GLenum Framebuffer::target() const
{
	if (read_only()) return GL_READ_FRAMEBUFFER;
	if (write_only()) return GL_DRAW_FRAMEBUFFER;
	return GL_FRAMEBUFFER;
}

void Framebuffer::create()
{
	if (_id != 0) destroy();
	glGenFramebuffers(1, &_id);
	LOG(INFO) << "Framebuffer created with ID " << _id;
}

void Framebuffer::destroy()
{
	if (_id == 0) return;
	glDeleteFramebuffers(1, &_id);
	_id = 0;
}

FramebufferStatus Framebuffer::status() const
{
	return (FramebufferStatus) glCheckFramebufferStatus(target());
}

void Framebuffer::attach_depth(Texture const& texture)
{
	glFramebufferTexture(target(), GL_DEPTH_ATTACHMENT, texture.id(), 0);
}

void Framebuffer::attach_color(Texture const& texture, glm::uint32 attachment)
{
	glFramebufferTexture(target(), (GLenum)((glm::uint32) GL_COLOR_ATTACHMENT0 + attachment), texture.id(), 0);
}
