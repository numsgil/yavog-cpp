#ifndef YAVOG_CPP_SAMPLER_HPP
#define YAVOG_CPP_SAMPLER_HPP

#include "Resource.hpp"

enum class MinFilter : int
{
	Nearest = 0x2600,
	Linear = 0x2601,
	NearestMipmapNearest = 0x2700,
	NearestMipmapLinear = 0x2702,
	LinearMipmapNearest = 0x2701,
	LinearMipmapLinear = 0x2703
};

enum class MagFilter : int
{
	Nearest = 0x2600,
	Linear = 0x2601
};

enum class Wrap : int
{
	ClampToEdge = 0x812F,
	MirroredRepeat = 0x8370,
	Repeat = 0x2901,
	MirrorClampToEdge = 0x8743
};

enum class CompareMode : int
{
	RefToTexture = 0x884E,
	None = 0
};

enum class CompareFunction : int
{
	LessEqual = 0x0203,
	GreaterEqual = 0x0206,
	Less = 0x0201,
	Greater = 0x0204,
	Equal = 0x0202,
	NotEqual = 0x0205,
	Always = 0x0207,
	Never = 0x0200
};

class Sampler : public Resource<glm::uint32>
{
public:
	Sampler();

	virtual ~Sampler();

	Sampler(Sampler const& other) = default;

	Sampler(Sampler&& other) = default;

	Sampler& operator=(Sampler const& other) = default;

	Sampler& operator=(Sampler&& other) = default;

	virtual void create();

	virtual void destroy();

	virtual void min_filter(MinFilter const& filter);

	virtual void mag_filter(MagFilter const& filter);

	virtual void min_lod(float value);

	virtual void max_lod(float value);

	virtual void wrap_s(Wrap const& value);

	virtual void wrap_t(Wrap const& value);

	virtual void wrap_r(Wrap const& value);

	virtual void border_color(glm::vec4 const& color);

	virtual void compare_mode(CompareMode const& mode);

	virtual void compare_function(CompareFunction const& func);
};

#endif //YAVOG_CPP_SAMPLER_HPP
