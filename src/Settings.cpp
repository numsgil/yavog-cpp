#include <g3log/g3log.hpp>
#include <streambuf>
#include <fstream>

#include "Utils/Json.hpp"
#include "Settings.hpp"

using namespace glm;
using namespace std;

Settings::Settings()
{
	_resolution = {960, 640};
	_fullscreen = false;
	_inverse_y = false;
	_sensitivity = 10;
}

void Settings::load()
{
	ifstream file("settings.js");
	if (!file.is_open())
	{
		save();
		return;
	}

	string err;
	Json js = Json::parse(string((istreambuf_iterator<char>(file)), istreambuf_iterator<char>()), err);
	CHECK(!js.is_null()) << "There was an error while loading the settings : " << err;

	_resolution.x = js.object_items().at("resolution").array_items().at(0).int_value();
	_resolution.y = js.object_items().at("resolution").array_items().at(1).int_value();
	_fullscreen = js.object_items().at("fullscreen").bool_value();
	_inverse_y = js.object_items().at("mouse").object_items().at("inverse_y").bool_value();
	_sensitivity = js.object_items().at("mouse").object_items().at("sensitivity").int_value();

	LOG(INFO) << "Settings loaded";
}

void Settings::save() const
{
	Json root = Json::object{
		{"resolution", Json::array{_resolution.x, _resolution.y}},
		{"fullscreen", _fullscreen},
		{"mouse",      Json::object{
			{"inverse_y",   _inverse_y},
			{"sensitivity", _sensitivity}
		}}
	};

	string js = root.dump();
	ofstream file("settings.js", ios::trunc);
	file.write(js.c_str(), js.size());
	LOG(INFO) << "Settings saved";
}
