#ifndef YAVOG_CPP_RENDERABLE_HPP
#define YAVOG_CPP_RENDERABLE_HPP

#include <anax/Component.hpp>
#include <g3log/g3log.hpp>
#include <vector>
#include <array>

#include "../Renderer.hpp"

class Renderable : public anax::Component
{
public:
	struct Material
	{
		Material() : texture(TextureTarget::Texture2D), specular(0.7f, 0.7f, 0.7f), shininess(16.f)
		{ }

		Texture texture;
		glm::vec3 specular;
		float shininess;
	};

	Renderable(Shader* shader)
			: _indexed(false), _data(BufferTarget::Array), _index(BufferTarget::ElementArray), _shader(shader),
			  _mode(PrimitiveMode::Triangles), _start(0), _count(0), _index_count(0), _index_type(IndexType::UnsignedByte),
			  _mat()
	{
		_data.create();
		_vao.create();
	}

	Renderable(Renderable const& other)
			: _indexed(other._indexed), _data(other._data), _index(other._index), _vao(other._vao), _shader(other._shader),
			  _mode(other._mode), _start(other._start), _count(other._count), _index_count(other._index_count), _index_type(other._index_type),
			  _mat(other._mat)
	{ }

	Renderable(Renderable&& other)
			: _indexed(other._indexed), _data(std::move(other._data)), _index(std::move(other._index)), _vao(std::move(other._vao)), _shader(other._shader),
			  _mode(other._mode), _start(other._start), _count(other._count), _index_count(other._index_count), _index_type(other._index_type),
			  _mat(std::move(other._mat))
	{
		other._indexed = false;
		other._shader = nullptr;

		other._mode = PrimitiveMode::Triangles;
		other._start = other._count = other._index_count = 0;
		other._index_type = IndexType::UnsignedByte;
	}

	Renderable& operator=(Renderable const& other)
	{
		_indexed = other._indexed;
		_data = std::move(other._data);
		_index = std::move(other._index);
		_vao = std::move(other._vao);
		_shader = other._shader;
		_mode = other._mode;
		_start = other._start;
		_count = other._count;
		_index_count = other._index_count;
		_index_type = other._index_type;
		_mat = other._mat;

		return *this;
	}

	Renderable& operator=(Renderable&& other)
	{
		_indexed = other._indexed;
		_data = std::move(other._data);
		_index = std::move(other._index);
		_vao = std::move(other._vao);
		_shader = other._shader;
		_mode = other._mode;
		_start = other._start;
		_count = other._count;
		_index_count = other._index_count;
		_index_type = other._index_type;
		_mat = std::move(other._mat);

		other._indexed = false;
		other._shader = nullptr;
		other._mode = PrimitiveMode::Triangles;
		other._start = other._count = other._index_count = 0;
		other._index_type = IndexType::UnsignedByte;

		return *this;
	}

	bool indexed() const
	{ return _indexed; }

	Buffer const& vertex_buffer() const
	{ return _data; }

	Buffer& vertex_buffer()
	{ return _data; }

	Buffer const& index_buffer() const
	{ return _index; }

	Buffer& index_buffer()
	{ return _index; }

	VertexArray const& vertex_array() const
	{ return _vao; }

	VertexArray& vertex_array()
	{ return _vao; }

	Shader const& shader() const
	{
		CHECK(_shader != nullptr) << "No shader assigned to the Renderable";
		return *_shader;
	}

	Shader& shader()
	{
		CHECK(_shader != nullptr) << "No shader assigned to the Renderable";
		return *_shader;
	}

	Renderable& set_range(glm::uint32 start, glm::uint32 count)
	{
		_start = start;
		_count = count;
		return *this;
	}

	glm::uint32 start() const
	{ return _start; }

	glm::uint32 count() const
	{ return _count; }

	glm::uint32 index_count() const
	{ return _index_count; }

	IndexType index_type() const
	{ return _index_type; }

	Renderable& mode(PrimitiveMode const& mode)
	{
		_mode = mode;
		return *this;
	}

	PrimitiveMode const& mode() const
	{ return _mode; }

	Renderable& material(Texture const& texture, glm::vec3 const& specular, float shininess)
	{
		_mat.texture = texture;
		_mat.specular = specular;
		_mat.shininess = shininess;
		return *this;
	}

	Material material() const
	{ return _mat; }

	template<typename T>
	Renderable& set_data(const std::vector<T>& data, BufferUsage usage)
	{
		_count = (glm::uint32) data.size();
		_data.set_data(data, usage);
        return *this;
	}

	template<typename T, std::size_t Size>
	Renderable& set_data(const std::array<T, Size>& data, BufferUsage usage)
	{
		_count = (glm::uint32) Size;
		_data.set_data(data, usage);
		return *this;
	}

	template<typename T>
	Renderable& set_indices(const std::vector<T>& data, BufferUsage usage, IndexType const& type)
	{
		if (!_indexed)
		{
			_indexed = true;
			_index.create();
			_vao.bind_index_buffer(_index);
		}
		_index_count = (glm::uint32) data.size();
		_index_type = type;
		_index.set_data(data, usage);
		return *this;
	}

	template<typename T, std::size_t Size>
	Renderable& set_indices(const std::array<T, Size>& data, BufferUsage usage, IndexType const& type)
	{
		if (!_indexed)
		{
			_indexed = true;
			_index.create();
			_vao.bind_index_buffer(_index);
		}
		_index_count = (glm::uint32) Size;
		_index_type = type;
		_index.set_data(data, usage);
		return *this;
	}

	Renderable& bind_buffer_attribute(glm::uint32 index, AttributeType const& type, int size, glm::uint32 stride, glm::uint32 offset, bool normalized = false)
	{
		_vao.bind_buffer_attribute(_data, index, type, size, stride, offset, normalized);
		return *this;
	}

protected:
	bool _indexed;
	Shader* _shader;
	Buffer _data, _index;
	VertexArray _vao;

	PrimitiveMode _mode;
	glm::uint32 _start, _count, _index_count;
	IndexType _index_type;

	Material _mat;
};

#endif //YAVOG_CPP_RENDERABLE_HPP
