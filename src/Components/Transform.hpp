#ifndef YAVOG_CPP_TRANSFORM_HPP
#define YAVOG_CPP_TRANSFORM_HPP

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/glm.hpp>

#include <anax/Component.hpp>

class Transform : public anax::Component
{
public:
	Transform(Transform* parent = nullptr)
		: _position(glm::vec3()), _rotation(glm::quat()), _scale(1), _parent(parent)
	{ }

	Transform(glm::vec3 const& position, Transform* parent = nullptr)
		: _position(position), _rotation(glm::quat()), _scale(1), _parent(parent)
	{ }

	Transform(glm::vec3 const& position, glm::quat const& rotation, Transform* parent = nullptr)
		: _position(position), _rotation(rotation), _scale(1), _parent(parent)
	{ }

	Transform(glm::vec3 const& position, glm::quat const& rotation, float scale, Transform* parent = nullptr)
		: _position(position), _rotation(rotation), _scale(scale), _parent(parent)
	{ }

	Transform(Transform const& other)
		: _position(other._position), _rotation(other._rotation), _scale(other._scale), _parent(other._parent)
	{
	}

	Transform(Transform&& other)
		: _position(other._position), _rotation(other._rotation), _scale(other._scale), _parent(other._parent)
	{
		other._position = glm::vec3();
		other._rotation = glm::quat();
		other._scale = 1;
		other._parent = nullptr;
	}

	Transform& operator=(Transform const& other)
	{
		_position = other._position;
		_rotation = other._rotation;
		_scale = other._scale;
		_parent = other._parent;

		return *this;
	}

	Transform& operator=(Transform&& other)
	{
		_position = other._position;
		_rotation = other._rotation;
		_scale = other._scale;
		_parent = other._parent;

		other._position = glm::vec3();
		other._rotation = glm::quat();
		other._scale = 1;
		other._parent = nullptr;

		return *this;
	}

	Transform* parent() const
	{ return _parent; }

	void parent(Transform* parent)
	{ _parent = parent; }

	glm::vec3 position() const
	{ return _position; }

	void position(glm::vec3 const& position)
	{ _position = position; }

	glm::quat rotation() const
	{ return _rotation; }

	void rotation(glm::quat const& rotation)
	{ _rotation = rotation; }

	glm::mat4 calculate() const
	{
		glm::mat4 parent(1.f);
		if (_parent != nullptr) parent = _parent->calculate();
		parent = glm::translate(parent, _position);
		parent *= glm::mat4_cast(_rotation);
		parent = glm::scale(parent, glm::vec3(_scale, _scale, _scale));

		return parent;
	}

protected:
	Transform* _parent;
	glm::vec3 _position;
	glm::quat _rotation;
	float _scale;
};

#endif //YAVOG_CPP_TRANSFORM_HPP
