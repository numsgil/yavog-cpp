#define GLFW_INCLUDE_NONE
#include "Engine.hpp"

#include <glbinding/Binding.h>
#include <GLFW/glfw3.h>

using namespace anax;
using namespace glm;

Engine* Engine::_inst = nullptr;

Engine& Engine::instance()
{
	CHECK(_inst != nullptr) << "Engine is not created";
	return *_inst;
}

Engine::Engine() : _world(nullptr), _pool(nullptr), _renderer(nullptr)
{
	_inst = this;

	_log = g3::LogWorker::createLogWorker();
	_log->addDefaultLogger("YAVoG", "./");
	g3::initializeLogging(_log.get());

	_settings = new Settings;
	_settings->load();
}

Engine::~Engine()
{
	_inst = nullptr;
	destroy();

	delete _settings;
	_settings = nullptr;
}

void Engine::create()
{
	LOG(INFO) << "Creating the Engine...";
	CHECK(glfwInit()) << "Could not initialize GLFW";

	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_RESIZABLE, 0);
	if (_settings->fullscreen())
	{
		GLFWmonitor* monitor = glfwGetPrimaryMonitor();
		const GLFWvidmode* mode = glfwGetVideoMode(monitor);

		glfwWindowHint(GLFW_RED_BITS, mode->redBits);
		glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
		glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
		glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
		_window = glfwCreateWindow(mode->width, mode->height, "YAVoG Rendering Window", monitor, 0);
	}
	else
		_window = glfwCreateWindow(_settings->resolution().x, _settings->resolution().y, "YAVoG Rendering Window", 0, 0);

	CHECK(_window) << "Could not create Window";
	glfwMakeContextCurrent(_window);

	glbinding::Binding::initialize();

	auto fb = framebuffer_size();

	glfwSwapInterval(1);
	glViewport(0, 0, fb.x, fb.y);
	glClearColor(0, 0, 0, 1);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_CCW);

	_caps = new Capabilities;
	_caps->log_all();

	LOG(INFO) << "Creating the Renderer...";
	_renderer = new Renderer;
	LOG(INFO) << "Creating the World...";
	_world = new AutoWorld;

	auto cores = std::thread::hardware_concurrency();
	if (cores < 1) cores = 1;
	if (cores > 1) cores -= 1;
	LOG(INFO) << "Creating the Thread Pool (" << cores << " threads)...";
	_pool = new ThreadPool(cores);

	LOG(INFO) << "Engine is running";
}

void Engine::destroy()
{
	LOG(INFO) << "Destroying the Thread Pool...";
	delete _pool;
	_pool = nullptr;

	LOG(INFO) << "Destroying the World...";
	delete _world;
	_world = nullptr;

	LOG(INFO) << "Destroying the Renderer...";
	delete _renderer;
	_renderer = nullptr;

	delete _caps;
	_caps = nullptr;

	LOG(INFO) << "Destroying the Engine...";
	glfwDestroyWindow(_window);
	glfwTerminate();

	LOG(INFO) << "Engine terminated";
}

void Engine::exit()
{
	glfwSetWindowShouldClose(_window, 1);
}

Settings& Engine::settings()
{
	return *_settings;
}

AutoWorld& Engine::world()
{
	CHECK(_world != nullptr) << "Engine is not created";
	return *_world;
}

Capabilities& Engine::capabilities()
{
	CHECK(_caps != nullptr) << "Engine is not created";
	return *_caps;
}

Renderer& Engine::renderer()
{
	CHECK(_renderer != nullptr) << "Engine is not created";
	return *_renderer;
}

bool Engine::is_running() const
{
	if (glfwWindowShouldClose(_window)) return false;

	glfwPollEvents();
	_world->refresh();
	return true;
}

void Engine::swap_buffers() const
{
	glfwSwapBuffers(_window);
}

bool Engine::is_key_down(Key const& code) const
{
	return glfwGetKey(_window, (int) code) == GLFW_PRESS;
}

MouseMode Engine::mouse_mode() const
{
	return (MouseMode) glfwGetInputMode(_window, GLFW_CURSOR);
}

void Engine::mouse_mode(MouseMode const& mode)
{
	glfwSetInputMode(_window, GLFW_CURSOR, (int) mode);
}

vec2 Engine::mouse_position() const
{
	double x, y;
	glfwGetCursorPos(_window, &x, &y);
	return vec2((float)x, (float)y);
}

bool Engine::is_mouse_down(Mouse const& mouse) const
{
	return glfwGetMouseButton(_window, (int) mouse) == GLFW_PRESS;
}

ivec2 Engine::window_size() const
{
	ivec2 ret;
	glfwGetWindowSize(_window, &ret.x, &ret.y);
	return ret;
}

ivec2 Engine::framebuffer_size() const
{
	ivec2 ret;
	glfwGetFramebufferSize(_window, &ret.x, &ret.y);
	return ret;
}

float Engine::time() const
{
	return (float) glfwGetTime();
}

AutoWorld::AutoWorld() : World()
{ }

AutoWorld::~AutoWorld()
{
	this->removeAllSystems();
	for (auto s : _systems)
		delete s.second;
	_systems.clear();
}
