#ifndef YAVOG_CPP_CAPABILITIES_HPP
#define YAVOG_CPP_CAPABILITIES_HPP

#include <glm/glm.hpp>

class Capabilities final
{
public:
	Capabilities();

	Capabilities(Capabilities const& other) = delete;

	Capabilities(Capabilities&& other) = delete;

	Capabilities& operator=(Capabilities const& other) = delete;

	Capabilities& operator=(Capabilities&& other) = delete;

	void log_all() const;

	int max_combined_texture_image_units() const;

	int max_cubemap_texture_size() const;

	int max_draw_buffers() const;

	int max_fragment_uniform_components() const;

	int max_texture_image_units() const;

	int max_texture_size() const;

	int max_varying_floats() const;

	int max_vertex_attributes() const;

	int max_vertex_texture_image_units() const;

	int max_vertex_uniform_components() const;

	glm::ivec2 max_viewport_dimensions() const;

	int max_uniform_block_size() const;

	int max_texture_buffer_size() const;

	int max_rectangle_texture_size() const;

	int max_array_texture_layers() const;
};

#endif //YAVOG_CPP_CAPABILITIES_HPP
