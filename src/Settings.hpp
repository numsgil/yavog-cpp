#ifndef YAVOG_CPP_SETTINGS_HPP
#define YAVOG_CPP_SETTINGS_HPP

#include <glm/glm.hpp>
#include <string>

class Settings final
{
public:
	Settings();

	Settings(Settings const& other) = delete;

	Settings(Settings&& other) = delete;

	Settings& operator=(Settings const& other) = delete;

	Settings& operator=(Settings&& other) = delete;

	void load();

	void save() const;

	glm::ivec2 resolution() const
	{ return _resolution; }

	void resolution(glm::ivec2 const& value)
	{ _resolution = value; }

	bool fullscreen() const
	{ return _fullscreen; }

	void fullscreen(bool value)
	{ _fullscreen = value; }

	bool mouse_inverse_y() const
	{ return _inverse_y; }

	void mouse_inverse_y(bool value)
	{ _inverse_y = value; }

	int mouse_sensitivity() const
	{ return _sensitivity; }

	void mouse_sensitivity(int value)
	{ _sensitivity = value; }

protected:
	glm::ivec2 _resolution;
	bool _fullscreen, _inverse_y;
	int _sensitivity;
};

#endif //YAVOG_CPP_SETTINGS_HPP
