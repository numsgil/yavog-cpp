#include "Renderer.hpp"

#include <glbinding/gl40core/gl.h>
#include <g3log/g3log.hpp>

using namespace gl40core;
using namespace glm;

Renderer::Renderer() : _cur_prog(0), _cur_vao(0), _cur_read_fbo(0), _cur_write_fbo(0)
{
}

Renderer::~Renderer()
{
}

void Renderer::clear(bool color, bool depth, bool stencil)
{
	ClearBufferMask bit = (ClearBufferMask) 0;
	if (color) bit |= GL_COLOR_BUFFER_BIT;
	if (depth) bit |= GL_DEPTH_BUFFER_BIT;
	if (stencil) bit |= GL_STENCIL_BUFFER_BIT;
	glClear(bit);
}

void Renderer::framebuffer(Framebuffer const& fbo)
{
	auto target = fbo.target();
	if (target == GL_DRAW_FRAMEBUFFER && _cur_write_fbo != fbo.id())
	{
		glBindFramebuffer(target, fbo.id());
		_cur_write_fbo = fbo.id();
	}
	if (target == GL_READ_FRAMEBUFFER && _cur_read_fbo != fbo.id())
	{
		glBindFramebuffer(target, fbo.id());
		_cur_read_fbo = fbo.id();
	}
	if (target == GL_FRAMEBUFFER && (_cur_write_fbo != fbo.id() || _cur_read_fbo != fbo.id()))
	{
		glBindFramebuffer(target, fbo.id());
		_cur_write_fbo = _cur_read_fbo = fbo.id();
	}
}

void Renderer::framebuffer()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	_cur_write_fbo = _cur_read_fbo = 0;
}

void Renderer::use(Buffer const& buf)
{
	glBindBuffer((GLenum) buf.target(), buf.id());
}

void Renderer::use(Shader const& prog)
{
	if (_cur_prog != prog.id())
	{
		glUseProgram(prog.id());
		_cur_prog = prog.id();
	}
}

void Renderer::use(Texture const& tex, glm::uint32 unit)
{
	glActiveTexture((GLenum)((glm::uint32) GL_TEXTURE0 + unit));
	glBindTexture((GLenum) tex.target(), tex.id());
}

void Renderer::use(Sampler const& sam, glm::uint32 unit)
{
	glBindSampler(sam.id(), unit);
}

void Renderer::use(VertexArray const& vao)
{
	if (_cur_vao != vao.id())
	{
		glBindVertexArray(vao.id());
		_cur_vao = vao.id();
	}
}

void Renderer::draw(PrimitiveMode const& mode, uint32 index, uint32 count)
{
	CHECK(_cur_prog != 0) << "Can't draw : no shader program bound";
	CHECK(_cur_vao != 0) << "Can't draw : no vertex array bound";
	glDrawArrays((GLenum) mode, index, count);
}

void Renderer::draw(PrimitiveMode const& mode, glm::uint32 count, IndexType const& type)
{
	CHECK(_cur_prog != 0) << "Can't draw : no shader program bound";
	CHECK(_cur_vao != 0) << "Can't draw : no vertex array bound";
	glDrawElements((GLenum) mode, count, (GLenum) type, 0);
}
